# Summary

Date : 2022-12-03 23:20:24

Directory /code/muffinOS_RSICV

Total : 62 files,  17693 codes, 584 comments, 1499 blanks, all 19776 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Assembler file | 1 | 12,394 | 5 | 504 | 12,903 |
| C | 55 | 5,066 | 541 | 931 | 6,538 |
| Makefile | 1 | 136 | 27 | 43 | 206 |
| C++ | 4 | 64 | 6 | 14 | 84 |
| LinkerScript | 1 | 33 | 5 | 7 | 45 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 62 | 17,693 | 584 | 1,499 | 19,776 |
| kernel | 42 | 16,416 | 514 | 1,286 | 18,216 |
| kernel/fs | 15 | 1,436 | 167 | 264 | 1,867 |
| mkfs | 1 | 239 | 14 | 51 | 304 |
| user | 18 | 902 | 29 | 119 | 1,050 |
| user/lib | 5 | 294 | 10 | 43 | 347 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)