# Diff Details

Date : 2022-12-03 23:21:12

Directory /code/muffinOS_RSICV

Total : 1 files,  -12394 codes, -5 comments, -504 blanks, all -12903 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [kernel/kernel.asm](/kernel/kernel.asm) | Assembler file | -12,394 | -5 | -504 | -12,903 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details