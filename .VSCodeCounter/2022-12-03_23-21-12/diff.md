# Diff Summary

Date : 2022-12-03 23:21:12

Directory /code/muffinOS_RSICV

Total : 1 files,  -12394 codes, -5 comments, -504 blanks, all -12903 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Assembler file | 1 | -12,394 | -5 | -504 | -12,903 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 1 | -12,394 | -5 | -504 | -12,903 |
| kernel | 1 | -12,394 | -5 | -504 | -12,903 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)