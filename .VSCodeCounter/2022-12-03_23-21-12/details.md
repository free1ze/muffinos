# Details

Date : 2022-12-03 23:21:12

Directory /code/muffinOS_RSICV

Total : 61 files,  5299 codes, 579 comments, 995 blanks, all 6873 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Makefile](/Makefile) | Makefile | 136 | 27 | 43 | 206 |
| [kernel/console.c](/kernel/console.c) | C | 86 | 4 | 15 | 105 |
| [kernel/defs.h](/kernel/defs.h) | C | 148 | 28 | 31 | 207 |
| [kernel/elf.h](/kernel/elf.h) | C++ | 36 | 5 | 7 | 48 |
| [kernel/exec.c](/kernel/exec.c) | C | 119 | 17 | 19 | 155 |
| [kernel/fs/bio.c](/kernel/fs/bio.c) | C | 86 | 2 | 16 | 104 |
| [kernel/fs/bio.h](/kernel/fs/bio.h) | C | 15 | 2 | 5 | 22 |
| [kernel/fs/dir.c](/kernel/fs/dir.c) | C | 41 | 32 | 9 | 82 |
| [kernel/fs/dir.h](/kernel/fs/dir.h) | C | 13 | 0 | 6 | 19 |
| [kernel/fs/fctrl.h](/kernel/fs/fctrl.h) | C | 9 | 0 | 3 | 12 |
| [kernel/fs/file.c](/kernel/fs/file.c) | C | 115 | 1 | 15 | 131 |
| [kernel/fs/file.h](/kernel/fs/file.h) | C++ | 16 | 0 | 3 | 19 |
| [kernel/fs/inode.c](/kernel/fs/inode.c) | C | 266 | 11 | 29 | 306 |
| [kernel/fs/inode.h](/kernel/fs/inode.h) | C | 36 | 4 | 10 | 50 |
| [kernel/fs/log.c](/kernel/fs/log.c) | C | 116 | 2 | 14 | 132 |
| [kernel/fs/log.h](/kernel/fs/log.h) | C | 33 | 5 | 14 | 52 |
| [kernel/fs/name.c](/kernel/fs/name.c) | C | 62 | 13 | 7 | 82 |
| [kernel/fs/sysfile.c](/kernel/fs/sysfile.c) | C | 380 | 2 | 60 | 442 |
| [kernel/fs/virtio.h](/kernel/fs/virtio.h) | C | 62 | 24 | 14 | 100 |
| [kernel/fs/virtio_disk.c](/kernel/fs/virtio_disk.c) | C | 186 | 69 | 59 | 314 |
| [kernel/kalloc.c](/kernel/kalloc.c) | C | 68 | 2 | 12 | 82 |
| [kernel/kernel.ld](/kernel/kernel.ld) | LinkerScript | 33 | 5 | 7 | 45 |
| [kernel/lock.c](/kernel/lock.c) | C | 100 | 9 | 19 | 128 |
| [kernel/lock.h](/kernel/lock.h) | C | 17 | 0 | 7 | 24 |
| [kernel/main.c](/kernel/main.c) | C | 44 | 30 | 12 | 86 |
| [kernel/memlayout.h](/kernel/memlayout.h) | C | 43 | 36 | 20 | 99 |
| [kernel/param.h](/kernel/param.h) | C | 14 | 0 | 2 | 16 |
| [kernel/pipe.c](/kernel/pipe.c) | C | 119 | 0 | 14 | 133 |
| [kernel/plic.c](/kernel/plic.c) | C | 32 | 8 | 8 | 48 |
| [kernel/print.c](/kernel/print.c) | C | 153 | 6 | 22 | 181 |
| [kernel/proc.c](/kernel/proc.c) | C | 343 | 37 | 65 | 445 |
| [kernel/proc.h](/kernel/proc.h) | C | 92 | 3 | 12 | 107 |
| [kernel/riscv.h](/kernel/riscv.h) | C | 275 | 39 | 58 | 372 |
| [kernel/start.c](/kernel/start.c) | C | 41 | 27 | 21 | 89 |
| [kernel/string.c](/kernel/string.c) | C | 46 | 2 | 6 | 54 |
| [kernel/syscall.c](/kernel/syscall.c) | C | 188 | 9 | 25 | 222 |
| [kernel/syscall.h](/kernel/syscall.h) | C | 28 | 0 | 4 | 32 |
| [kernel/trap.c](/kernel/trap.c) | C | 132 | 27 | 43 | 202 |
| [kernel/types.h](/kernel/types.h) | C | 18 | 0 | 8 | 26 |
| [kernel/uart.c](/kernel/uart.c) | C | 101 | 23 | 30 | 154 |
| [kernel/vargs.h](/kernel/vargs.h) | C++ | 7 | 0 | 3 | 10 |
| [kernel/vm.c](/kernel/vm.c) | C | 303 | 25 | 48 | 376 |
| [mkfs/mkfs.c](/mkfs/mkfs.c) | C | 239 | 14 | 51 | 304 |
| [user/cat.c](/user/cat.c) | C | 29 | 0 | 3 | 32 |
| [user/echo.c](/user/echo.c) | C | 13 | 0 | 1 | 14 |
| [user/fact.c](/user/fact.c) | C | 50 | 3 | 7 | 60 |
| [user/init.c](/user/init.c) | C | 38 | 2 | 7 | 47 |
| [user/kill.c](/user/kill.c) | C | 0 | 0 | 1 | 1 |
| [user/lib/dpath.h](/user/lib/dpath.h) | C++ | 5 | 1 | 1 | 7 |
| [user/lib/ulib.c](/user/lib/ulib.c) | C | 89 | 1 | 9 | 99 |
| [user/lib/umalloc.c](/user/lib/umalloc.c) | C | 49 | 0 | 5 | 54 |
| [user/lib/uprintf.c](/user/lib/uprintf.c) | C | 104 | 2 | 17 | 123 |
| [user/lib/user.h](/user/lib/user.h) | C | 47 | 6 | 11 | 64 |
| [user/ls.c](/user/ls.c) | C | 66 | 0 | 6 | 72 |
| [user/mkdir.c](/user/mkdir.c) | C | 14 | 0 | 1 | 15 |
| [user/ps.c](/user/ps.c) | C | 15 | 0 | 2 | 17 |
| [user/rm.c](/user/rm.c) | C | 15 | 0 | 3 | 18 |
| [user/sh.c](/user/sh.c) | C | 291 | 13 | 34 | 338 |
| [user/slowprt.c](/user/slowprt.c) | C | 18 | 0 | 2 | 20 |
| [user/test.c](/user/test.c) | C | 13 | 1 | 3 | 17 |
| [user/wc.c](/user/wc.c) | C | 46 | 0 | 6 | 52 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)