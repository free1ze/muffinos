# Summary

Date : 2022-12-03 23:21:12

Directory /code/muffinOS_RSICV

Total : 61 files,  5299 codes, 579 comments, 995 blanks, all 6873 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C | 55 | 5,066 | 541 | 931 | 6,538 |
| Makefile | 1 | 136 | 27 | 43 | 206 |
| C++ | 4 | 64 | 6 | 14 | 84 |
| LinkerScript | 1 | 33 | 5 | 7 | 45 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 61 | 5,299 | 579 | 995 | 6,873 |
| kernel | 41 | 4,022 | 509 | 782 | 5,313 |
| kernel/fs | 15 | 1,436 | 167 | 264 | 1,867 |
| mkfs | 1 | 239 | 14 | 51 | 304 |
| user | 18 | 902 | 29 | 119 | 1,050 |
| user/lib | 5 | 294 | 10 | 43 | 347 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)