#include "./lib/user.h"
#include "kernel/fs/fctrl.h"


char *argv[] = { "sh", 0 };

int main() 
{
    // int fd = 0;
    if(open("console", O_RDWR) < 0){
        if(mknod("console", CONSOLE, 0) < 0) {
            kprint("mknod error\n");
        }
        if(open("console", O_RDWR) < 0 ) {
            kprint("open error\n");
        }
    }
    dup(0);  // stdout
    dup(0);  // stderr

    for(;;)
    {
        int pid = fork();
        if(pid == 0) {
            exec("sh", argv);
            printf("init: exec sh failed\n");
            exit(1);
        } else if (pid < 0){
            printf("init: fork failed\n");
            exit(1);
        }


        for(;;) {
            int wpid = wait(0);
            if(wpid == pid) {
                // restart sh
                break;
            } else if(wpid < 0) {
                printf("init: wait returned error\n");
                exit(1);
            }
        }
        
    }
    return 0;
}