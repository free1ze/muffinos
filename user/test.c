// test malloc and free
#include "./lib/user.h"
#include "../kernel/net/net.h"

void writer(int fd)
{
    for(int i=0; i<10; i++) {
        // fprintf(fd, "%d\n", i);
        fprintf(2, "%d\n", i);
        sleep(3);
    }
}

int main(int argc, char *argv[])
{   
    int fd = open("./out", O_CREATE|O_RDWR);

    if(fork() == 0) {
        int fd = open("./out", O_CREATE|O_RDWR|O_APPEND);
        writer(fd);
        exit(0);
    }

    // // writer(fd);
    printf("%d", fd);

    // wait(0);
    exit(0);
}
