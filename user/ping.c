#include "./lib/user.h"
#include "../kernel/net/net.h"


// dig.c
extern void dns(char *domain, struct dns_result *dns_rs);

static
uint16 checksum(void *head, int len)
{
    char *p = head;
    uint32 sum = 0;
    for(int i=0; i<len; i+=2,p+=2) {
        sum += (*p << 8) + (*(p+1));
    }
    if(len % 2 != 0) {
        char *last = ((char *)head) + len;
        sum += (*last << 8);
    }
    uint32 res = (sum >> 16) + (sum & 0x0000FFFF);
    uint16 checksum = ~res;
    return checksum;
}

void ping_job(int fd, uint32 dst_ip)
{
const int N = 100;
    uint8 obuf[N];
    memset(obuf, 0, N);

    int attempts = 6;
    int tot = 0, recv = 0;
  
    struct icmp *icmphdr = (struct icmp *)obuf;
    int len = sizeof(*icmphdr);
    icmphdr->type = ICMP_PING;
    icmphdr->code = 0;
    icmphdr->id = htons(0);

    for(int i=1; i<=attempts; i++) {

      icmphdr->seq = htons(i);

      // clear check sum!! really important !!!
      icmphdr->checksum = 0;
      icmphdr->checksum = htons(checksum(icmphdr, sizeof(*icmphdr)));

      if( write(fd, obuf, len) < 0 ) {
          error("ping: send() failed\n");
          exit(1);
      }
      tot++;

      int cc;
      uint8 ibuf[N];
      memset(ibuf, 0, N);
      if( ( cc = read(fd, ibuf, sizeof(ibuf)) ) < 0) {
        error("ping: recv() failed\n");
        exit(1);
      }
      struct icmp *recv_ihdr = (struct icmp *)ibuf;
      if(recv_ihdr->code != 0 || recv_ihdr->type != 0 ) {
        continue;
      }
      uint8 *ttl = (uint8 *)mov_p(recv_ihdr, 8);
      recv++;
      printf("%d bytes from %d.%d.%d.%d: icmp_seq=%d ttl=%d\n", 
        cc, dst_ip>>24, dst_ip>>16&0xFF, dst_ip>>8&0xFF, dst_ip&0xFF, ntohs(recv_ihdr->seq), *ttl);
    }

    printf("%d packets transmitted, %d received, %d packet loss\n", tot, recv, tot-recv);

    close(fd);
}

void ping_with_timer(int fd, uint32 dst_ip, uint32 tick)
{
    int pi[2];
    if(pipe(pi) < 0) {
      error("ping: pipe failed\n");
      exit(1);
    }

    if( fork() == 0 ) {
      ping_job(fd, dst_ip);
      write(pi[1], "1", 1);
      close(pi[0]);
      close(pi[1]);
      exit(0);
    }

    if( fork() == 0 ) {
      // timer sleep
      sleep(tick);
      write(pi[1], "2", 1);
      close(pi[0]);
      close(pi[1]);
      exit(0);
    }
    // either timer or job exits
    wait(0);
    char c;
    if(read(pi[0], &c, 1) > 0 && c == '2') {
      error("timeout\n");
    }
}

static void ping(char *domain)
{
    // try dns
    struct dns_result dns_rs[10];
    memset(dns_rs, 0, sizeof(dns_rs));

    dns(domain, dns_rs);

    if(dns_rs[0].ip == 0) {
      exit(1);
    }
    uint32 dst_ip = dns_rs[0].ip;
    int fd;
    if( (fd = connect(dst_ip, 0, 0, IPPROTO_ICMP)) < 0 ) {
        exit(1);
    }

    ping_with_timer(fd, dst_ip, 30);
}

int main(int argc, char *argv[])
{
    if(argc < 2 || argc > 2){
        error("usage: ping [ip]\n");
        exit(1);
    }

    ping(argv[1]);

    exit(0);
}