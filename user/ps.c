#include "./lib/user.h"

char buf[512];
void ps()
{
    procdump();
}

int main(int argc, char *argv[])
{
    if(argc == 1){
        ps();
    } else if(argc > 2){
        error("ps: too many arguments\n");
    } 
    exit(0);
}