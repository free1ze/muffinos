#include "./lib/user.h"

char buf[512];
void slowprt()
{
    for(int i=0; i<10; i++) {
        printf("%d\n", i);
        sleep(5);
    }
}

int main(int argc, char *argv[])
{
    if(argc == 1){
        slowprt();
    } else if(argc >= 2){
        error("slowprt: too many arguments\n");
    } 
    exit(0);
}