#include "./lib/user.h"

int main(int argc, char *argv[])
{
    if(argc < 2){
        error("usage: rm files...\n");
        exit(1);
    }

    for(int i = 1; i < argc; i++){
        if(unlink(argv[i]) < 0){
            error("rm: failed to delete %s\n", argv[i]);
            break;
        }
    }

    exit(0);
}