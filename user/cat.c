#include "./lib/user.h"
#include "../kernel/fs/dir.h"
#include "../kernel/fs/inode.h"

char buf[512];
void cat(char *path)
{
    int fd;
    if((fd = open(path, O_RDONLY)) < 0) {
        error("cat: cannot open %s\n", path);
        return;
    }
    struct stat st;
    if(fstat(fd, &st) < 0) {
        error("cat: cannot stat %s\n", path);
        close(fd);
        return ;
    }
    if(st.type != T_FILE) {
        error("cat: %s not a file\n", path);
        close(fd);
        return ;
    }
    int n;
    while ((n = read(fd, buf, sizeof(buf))) > 0)
    {
        if(write(1, buf, n) != n) {
            error("cat: write error %s\n", path);
            close(fd);
            return;
        }
    }
    close(fd);
}

int main(int argc, char *argv[])
{
    if(argc == 1){
        error("usage: cat [filename]\n");
    } else if(argc > 2){
        error("cat: too many arguments\n");
    } else {
        cat(argv[1]);
    }
    exit(0);
}