#include "./lib/user.h"
#include "../kernel/fs/dir.h"
#include "../kernel/fs/inode.h"

char *types[] = {
    "unknown",
    "T_DIR"  ,
    "T_FILE" ,
    "T_DEV" 
};

int stat(char *path, struct stat *st)
{
    int ffd = open(path, O_RDONLY);
    if(ffd < 0) {
        return -1;
    }
    fstat(ffd, st);
    close(ffd);
    return 0;
}

void ls(char *path)
{
    char buf[512];
    int fd;
    struct dirent de;
    struct stat st;

    if((fd = open(path, 0)) < 0){
        error("ls: cannot open %s\n", path);
        exit(0);
    }
    if(fstat(fd, &st) < 0) {
        error("ls: cannot stat %s\n", path);
        goto fail;
    }
    if(st.type != T_DIR) {
        error("ls: %s not a file\n", path);
        goto fail;
    }

    int len = strlen(path);
    strcpy(buf, path);
    buf[len-1] = '/';
    while( read(fd, &de, sizeof(de)) == sizeof(de) ) {
        if(de.inum == 0) {
            continue;
        }
        strcpy(buf + len, de.name);
        if(stat(buf, &st) < 0) {
            error("ls: cannot stat %s\n", buf);
            goto fail;
        }
        printf("%s\t\t%s\t\t%d\n",de.name, types[st.type], st.size);
    }
    fail:
    close(fd);
    exit(0);
}

int main(int argc, char *argv[])
{
    if(argc == 1){
        ls("./");
    } else if(argc == 2) {
        ls(argv[1]);
    } else {
        printf("ls: too many arguments\n");
    }
    exit(0);
}