#include "./lib/user.h"

void writetest()
{
    for(int k=0; k<50; k++)
    {
        int fd;
        if( (fd = open("temp", O_CREATE | O_RDWR)) < 0) {
            printf("cannot create file\n");
            exit(1);
        }
        for(int i=0; i<100; i++) {
            const char *alphabet = "abcdefghijklmnopqrstuvwxyz";
            write(fd, alphabet, strlen(alphabet));
        }
        close(fd);
    }
}

int fact(int n)
{
    if(n <= 1) return 1;
    return n * fact(n-1);
}

void caltest(int p)
{
    int k;
    for(k=0; k<p; k++) {
        if(fork() == 0) {
            for(int i=0; i<10000 / p; i++) {
                for(int j=0; j<1000; j++) {
                    fact(100);
                }
            }
            exit(0);
        }
    } 
    for(k=0; k<p; k++) {
        wait(0);
    }
}

int
main(int argc, char *argv[])
{
    int p_num = 7;
    uint64 before = gettick();
    // writetest();
    caltest(p_num);
    uint64 after = gettick();
    printf("used %d ticks\n", after - before);
    exit(0);
}
