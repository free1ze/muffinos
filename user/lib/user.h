#include "kernel/types.h"
#include "kernel/fs/fctrl.h"
#include "kernel/param.h"

#define CONSOLE 1

#define assert(x)     do {if(!(x)) { \
    printf("user assertion failed: (%s)\n", #x); \
    exit(1); \
}}while(0)

#define NELEM(p)    (sizeof(p) / sizeof(*p))

struct stat;
struct dns_result;

// struct rtcdate;

// system calls
int fork(void);
int exit(int) __attribute__((noreturn));
int wait(int*);
int pipe(int*);
int write(int, const void*, int);
int read(int, void*, int);
int close(int);
int kill(int);
int exec(char*, char**);
int open(const char*, int);
int mknod(const char*, short, short);
int unlink(const char*);
int fstat(int fd, struct stat*);
int link(const char*, const char*);
int mkdir(const char*);
int chdir(const char*);
int dup(int);
int getpid(void);
char* sbrk(int);
int sleep(int);
int uptime(void);
void procdump(void);
int connect(int, int, int, int);
uint64  gettick();

// ulib.c
// int stat(const char*, struct stat*);
// void *memmove(void*, const void*, int);
char* strchr(const char*, char c);
void fprintf(int, const char*, ...);
void printf(const char*, ...);
void error(const char*, ...);
char* gets(char*, int max);
void* malloc(uint);
void free(void*);
int atoi(const char*);

void memcpy(char *dst, const char *src, int len);
void memset(void *dst, char val, int len);
int strcmp(const char *p, const char *q);
char *strcpy(char *dst, const char *src);
char *strcat(char *dst, const char *src);
int strlen(const char *str);

// kernel print
void kprint(char *);

// dig.c
void dns(char *domain, struct dns_result *dns_rs);



