#include "user.h"

struct block
{
    uint size;
    int free;                       // if is in use
    void *data;
    struct block *next;
} __attribute__((aligned(8)));

static struct block *head = NULL;

void *malloc(uint size)
{
    if(size <= 0) {
        return 0;
    }

    struct block *p = head;
    while(p != NULL)
    {
        if(p->size >= size && p->free == true) {
            p->free = false;
            return p->data;
        }
        p = p->next;
    }
    p = (struct block *)sbrk(sizeof(struct block));
    if(p == (struct block *)-1) {
        return 0;
    }
    p->data = sbrk(size);
    if(p == (struct block *)-1) {
        return 0;
    }
    p->free = false;
    p->size = size;
    p->next = head;
    head = p;
    return p->data;
}

void free(void* addr)
{
    struct block *p = head;
    while(p != NULL)
    {
        if(p->data == addr && p->free == false) {
            p->free = true;
            return;
        }
        p = p->next;
    }
}