#include "user.h"
#include "kernel/net/net.h"

void encode_qname(uint8 *buf, char *domain)
{
    uint8 *p = buf;
    int count = 0;
    int i;
    for(i=0; i< strlen(domain); i++) {
        if(domain[i] == 0) {
            break;
        }
        if(domain[i] != '.') {
            count++;
            buf[i+1] = domain[i];
        } else {
            *p = count;
            count = 0;
            p = buf + i + 1;
        }
    }
    *p = count;
    buf[++i] = 0;
}

void decode_qname(char *qname)
{
    int len = strlen(qname) - 1;
    for(int i=1; i<len; i++) {
        if(qname[i] >= 0 && qname[i] <= 9) {
            qname[i] = '.';
        }
    }
}

int dns_req(uint8 *obuf, char *domain)
{
    int len = sizeof(struct dns);
    
    struct dns *hdr = (struct dns *) obuf;
    hdr->id = htons(6828);
    hdr->rd = 1;
    hdr->qdcount = htons(1);
    
    // qname part of question
    uint8 *qname = obuf + sizeof(struct dns);
    encode_qname(qname, domain);
    len += strlen(domain)+1;

    // constants part of question
    struct dns_question *h = (struct dns_question *) (qname+strlen(domain)+1);
    h->qtype = htons(0x1);
    h->qclass = htons(0x1);

    len += sizeof(struct dns_question);
    return len;
}

int dns_recv(uint8 *ibuf, int len, char *domain, struct dns_result *dns_rs)
{
    struct dns *hdr = (struct dns *) ibuf;

    if(len < sizeof(struct dns)){
        // error("DNS reply too short\n");
        return -1;
    }

    if(!hdr->qr) {
        // error("DNS not reply\n");
        return -1;
    }
    if(hdr->id != htons(6828)) {
        // error("DNS wrong id: %d\n", ntohs(hdr->id));
        return -1;
    }
    if(hdr->rcode != 0) {
        // error("DNS rcode error: %x\n", hdr->rcode);
        return -1;
    }

    char *qname = (char *)mov_p(ibuf, sizeof(struct dns));
    decode_qname(qname);
    if(strcmp(qname+1, domain) != 0) {
        // error("DNS wrong domain: %s\n", qname+1);
        return -1;
    }
    struct dns_question *h = (struct dns_question *)mov_p(qname, strlen(domain)+1);
    if(h->qtype != ntohs(0x1) || h->qclass != ntohs(0x1)) {
        // error("DNS header wrong\n");
        return -1;
    }
    
    struct dns_data *data = (struct dns_data *)mov_p(h, sizeof(*h));

    for(int i=0; i<ntohs(hdr->ancount); i++) {
        char *first = (char *)data;
        if( *first > 63) {  
            // dns domain compression
            data = mov_p(data, 2);
        } else {
            data = mov_p(data, strlen(domain) + 1);
        }

        if( ntohs(data->type) == ARECORD && ntohs(data->len) == 4) {
            char *ip = (char *)mov_p(data, sizeof(*data));
            dns_rs->ip = ntohl(*((uint32 *)ip));
            dns_rs->ttl = ntohl(data->ttl);
            dns_rs->type = ntohs(data->type);
            // printf("%s\t%d\t%d\t%d.%d.%d.%d\n", domain, ntohl(data->ttl), ntohs(data->type), ip[0], ip[1], ip[2], ip[3]);
            data = mov_p(data, sizeof(*data) + 4);
            dns_rs++;
        } else {
            break;
        }
    }

    return 0;
}

// save result int ips
void dns(char *domain, struct dns_result *dns_rs)
{
    const int N = 100;
    uint8 ibuf[N];
    uint8 obuf[N];
    memset(obuf, 0, N);
    memset(ibuf, 0, N);
  
    // 8.8.8.8: google's name server
    uint32 dst = (8 << 24) | (8 << 16) | (8 << 8) | (8 << 0);

    int fd;
    if( (fd = connect(dst, 53, 10000, IPPROTO_UDP)) < 0 ) {
        error("dig: cannot connect to dns server\n");
        exit(1);
    }

    int len = dns_req(obuf, domain);
    if(write(fd, obuf, len) < 0) {
        error("dig: send() failed\n");
        exit(1);
    }

    int cc;
    if( (cc = read(fd, ibuf, sizeof(ibuf))) < 0) {
        error("dig: recv() failed\n");
        exit(1);
    }

    if( dns_recv(ibuf, cc, domain, dns_rs) < 0 ) {
        error("cannot get ip address of %s\n", domain);
    }
    close(fd);
}
