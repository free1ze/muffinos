#include "user.h"

void memcpy(char *dst, const char *src, int len)
{
    assert(len > 0);
    while( len-- > 0) {
        *(dst++) = *(src++);
    }
}

void memset(void *dst, char val, int len)
{
    assert((len >= 0));
    char *d = dst;
    while( len-- > 0) {
        *(d++) = val;
    }
}

int strcmp(const char *p, const char *q) 
{
    while(*p && *q && (*p == *q)) {
        p++;
        q++;
    }
    return *p - *q;
}

char *strcpy(char *dst,const char *src)
{
    memcpy(dst, src, strlen(src));
    return dst;
}

char *strcat(char *dst, const char *src)
{
    int dlen = strlen(dst);
    int slen = strlen(src);
    for(int i=0; i<slen; i++) {
        dst[i+dlen] = src[i];
    }
    return dst;
}

int strlen(const char *str)
{
    int len = 0;
    while(*str++){
        len++;
    }
    return len+1;
}

// return 0 on error
int atoi(const char* buf)
{
    int count = 0;
    int sign = 1;
    char *next = (char *)buf;
    if(buf[0] == '-') {
        sign = -1;
        next++;
    }
    while (*next != 0)
    {
        int num = *next++ - '0';
        if(num < 0 || num > 9) {
            return 0;
        }
        count = count * 10 + num;
    }
    return count * sign;
}

char* gets(char* buf, int max)
{
    char c;
    int i=0, cc;
    for(; i<max; ) {
        cc = read(0, &c, 1);
        if(cc < 1) {
          break;
        }
        if(c == '\n' || c == '\r') {
            break;
        }
        buf[i++] = c;
    }
    buf[i] = '\0';
    return buf;
}

char* strchr(const char *s, char c)
{
  for(; *s; s++)
    if(*s == c)
      return (char*)s;
  return 0;
}