#include "./lib/user.h"

int main(int argc, char *argv[])
{
    if(argc == 1) {
        error("mkdir: too many arguments\n");
    } else if(argc >= 3){
        error("usage: mkdir [dirname]\n");
    } else {
        if(mkdir(argv[1]) < 0) {
            error("mkdir: cannot create %s\n", argv[1]);
        }
    }
    exit(0);
}