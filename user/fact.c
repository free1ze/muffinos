// read numbers x from input
// output x! 
// stop if read 0
#include "./lib/user.h"

int fact(int n)
{
    if (n > 1)
    {
        return n * fact(n - 1);
    }
    return 1;
}

int safeatio(char *buf)
{
    int i=0;
    while (1)
    {
        if(buf[i] == '\n' || buf[i] == 0) {
            buf[i] = 0;
            return atoi(buf);
        }
        if(buf[i] < '0' || buf[i] > '9') {
            printf("%s not a number\n", buf);
            return 0;
        }
        i++;
    }
    
}

int main(int argc, char *argv[])
{
    if(argc > 1){
        error("fact: too many arguments\n");
    } else {
        int res;
        char buf[10];
        
        while (1)
        {
            printf("enter n(0 to quit): ");
            if(read(0, buf, sizeof(buf)) <= 0) {
                exit(0);
            }
            res = safeatio(buf);
            if(res < 0) {
                error("n must be greater than 0.\n");
            } else if( res == 0 ){
                exit(0);
            } else {
                printf("The factorial of %d is %d\n", res, fact(res));
            }
        }
        
    }
    exit(0);
}
