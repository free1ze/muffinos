#include "./lib/user.h"
#include "./lib/dpath.h"
#include "../kernel/fs/dir.h"


enum {
    UNKNOWN = 0,
    ERROR,
    EXEC,
    REDIR,
    PIPE,
    BACK,
};

#define MAX_ARGC 10
#define MAX_ARG_LEN 80

// hold shell input
char shbuf[MAX_ARGC * MAX_ARG_LEN];

// hold arg parsing result
char argvbuf[MAX_ARGC * MAX_ARG_LEN];
char *argv[MAX_ARGC];

// hold pwd
int pwdlen = 0;
char pwd[10][10];

struct cmd
{
    int type;
    void *ncmd;
};

struct execcmd
{
    char *argv[MAX_ARGC];
};

struct redircmd
{
    int fd;
    int flag;
    char *path;
    struct execcmd *cmd;
};

struct pipecmd
{
    struct execcmd *lcmd;
    struct execcmd *rcmd;
};

struct bgcmd
{
    struct cmd *cmd;
};

void panic(char *msg)
{
    error("[user panic]: %s\n", msg);
    for(;;)
        ;
}

void *safe_malloc(uint sz)
{
    void *p = malloc(sz);
    if(p == 0) {
        error("malloc failed\n");
        exit(1);
    }
    memset(p, 0, sz);
    return p;
}

int safe_fork(void)
{
  int pid = fork();
  if(pid == -1) {
    error("fork failed\n");
    exit(1);
  }
  return pid;
}

// parse cmd buffer, split args by ' '
// save cmd all to argv
// return -1 on error
static int parse_shbuf()
{
    memset(argv, 0, sizeof(argv));    
    memset(argvbuf, 0, sizeof(argvbuf));
    
    char *p = shbuf;
    char *q = argvbuf;
    int i = 0;
    while(1){
        argv[i] = q;
        int len = 0;
        while(*p != ' ' && *p != 0) {
            *q++ = *p++; 
            if(len++ > MAX_ARG_LEN) {
                error("sh: single argument too long\n");
                return -1;
            }
        }
        *q++ = 0;
        if(*p == 0) {
            break;
        }
        while (*p == ' ')
        {
            p++;
        }
        i++;
    }
    return 0;
}

void showpwd()
{
    if( pwdlen == 0 ){
        fprintf(2, "/");
    }
    for(int i=0; i<pwdlen; i++) {
        fprintf(2, "/%s",pwd[i]);
    }
}

static char *skipelem(char *path, char *name)
{
    while(*path == '/') {
        path++;
    }
    if(*path == 0) {
        return 0;
    }
    while(*path != '/' && *path != 0) {
        *name++ = *path++;
    }
    *name++ = 0;
    return path;
}

void cd()
{
    if(!shbuf[3]) {
        error("usage: cd [dirname]\n");
        return;
    }
    if(strlen(&shbuf[3]) > DIRSIZ) {
        error("cd: path too long\n");
        return;
    }
    
    if( chdir(&shbuf[3]) < 0) {
        error("cd: cannot cd to %s\n", &shbuf[3]);
    } else {
        char *p = &shbuf[3];
        while (( p = skipelem(p, pwd[pwdlen])) != 0)
        {
            if(strcmp(pwd[pwdlen], ".") == 0) {
                continue;
            } else if (strcmp(pwd[pwdlen], "..") == 0) {
                if(pwdlen > 0) pwdlen--;
            } else {
                pwdlen++;
            }
        }
    }
}

static struct cmd* parse_exec()
{
    struct execcmd *ccmd = safe_malloc(sizeof(struct execcmd));
    for(int i = 0; i < NELEM(argv); i++) {
        ccmd->argv[i] = argv[i];
    }
    struct cmd *cmd = safe_malloc(sizeof(struct cmd));
    cmd->ncmd = ccmd;
    cmd->type = EXEC;
    return cmd;
}

static struct cmd* parse_redir()
{
    for(int i=1; i<NELEM(argv); i++) {
        if(strcmp(argv[i], "<") == 0) {
            struct redircmd *rcmd = safe_malloc(sizeof(struct redircmd));
            rcmd->fd = 0;
            rcmd->flag = O_RDONLY;
            rcmd->path = argv[i+1];
            struct execcmd *ccmd = safe_malloc(sizeof(struct execcmd));
            for(int j = 0; j < i; j++) {
                ccmd->argv[j] = argv[j];
            }
            rcmd->cmd = ccmd;
            struct cmd *cmd = safe_malloc(sizeof(struct cmd));
            cmd->type = REDIR;
            cmd->ncmd = rcmd;
            return cmd;
        }
        else if(strcmp(argv[i], ">") == 0) {
            struct redircmd *rcmd = safe_malloc(sizeof(struct redircmd));
            rcmd->fd = 1;
            rcmd->flag = O_WRONLY|O_CREATE|O_TRUNC;
            rcmd->path = argv[i+1];
            struct execcmd *ccmd = safe_malloc(sizeof(struct execcmd));
            for(int j = 0; j < i; j++) {
                ccmd->argv[j] = argv[j];
            }
            rcmd->cmd = ccmd;
            struct cmd *cmd = safe_malloc(sizeof(struct cmd));
            cmd->type = REDIR;
            cmd->ncmd = rcmd;
            return cmd;
        }
        else if(strcmp(argv[i], ">>") == 0) {
            struct redircmd *rcmd = safe_malloc(sizeof(struct redircmd));
            rcmd->fd = 1;
            rcmd->flag = O_WRONLY |O_CREATE| O_APPEND;
            rcmd->path = argv[i+1];
            struct execcmd *ccmd = safe_malloc(sizeof(struct execcmd));
            for(int j = 0; j < i; j++) {
                ccmd->argv[j] = argv[j];
            }
            rcmd->cmd = ccmd;
            struct cmd *cmd = safe_malloc(sizeof(struct cmd));
            cmd->type = REDIR;
            cmd->ncmd = rcmd;
            return cmd;
        } 
        else if(strcmp(argv[i], "|") == 0) {
            struct pipecmd *pcmd = safe_malloc(sizeof(struct pipecmd));
            struct execcmd *lcmd = safe_malloc(sizeof(struct execcmd));
            struct execcmd *rcmd = safe_malloc(sizeof(struct execcmd));
            for(int j = 0; j < i; j++) {
                lcmd->argv[j] = argv[j];
            }
            for(int j = i+1; j < NELEM(argv); j++) {
                rcmd->argv[j-i-1] = argv[j];
            }
            pcmd->lcmd = lcmd;
            pcmd->rcmd = rcmd;

            struct cmd *cmd = safe_malloc(sizeof(struct cmd));
            cmd->type = PIPE;
            cmd->ncmd = pcmd;
            return cmd;
        }
    }
    return NULL;
}

static struct cmd* parse_bg()
{
    for(int i=NELEM(argv)-1; i>=0; i--) {
        if(argv[i] == 0) {
            continue;
        } else {
            if(strcmp(argv[i], "&") == 0) {
                argv[i] = 0;
                struct cmd *cmd = safe_malloc(sizeof(struct cmd));
                cmd->type = BACK;
                struct bgcmd *bcmd = safe_malloc(sizeof(struct bgcmd));
                cmd->ncmd = bcmd;
                bcmd->cmd = parse_redir();
                if(bcmd->cmd == NULL) {
                    bcmd->cmd = parse_exec();
                }
                return cmd;
            }
            break;
        }
    }
    return NULL;
}


// alloc cmd and save to cmd pointer
// return type
struct cmd* parse_cmd()
{
    if( parse_shbuf() < 0 ) {
        return NULL;
    }

    struct cmd *cmd = NULL;
    if( (cmd = parse_bg()) != NULL) {
        return cmd;
    }
    if( (cmd = parse_redir()) != NULL) {
        return cmd;
    }
    return parse_exec();
}

void exec_cmd(struct execcmd *cmd)
{
    char path_buf[MAXPATH];

    // try path "./"
    exec(cmd->argv[0], cmd->argv);
    // try each default path
    for(int i=0; i<MAX_DEFAULT_PATH; i++) {
        if(strcmp(dpath[i], 0) == 0) {
            break;
        }
        strcpy(path_buf, dpath[i]);
        strcpy(path_buf + strlen(dpath[i])-1, cmd->argv[0]);
        exec(path_buf, cmd->argv);
    }
    error("exec %s failed\n",cmd->argv[0]);
}

void run_cmd(struct cmd *cmd){
    // struct execcmd *ccmd;
    struct redircmd *rcmd;
    struct pipecmd *pcmd;
    struct bgcmd *bcmd;
    int pi[2];

    switch (cmd->type)
    {
        case REDIR:
            rcmd = cmd->ncmd;
            close(rcmd->fd);
            if( open(rcmd->path, rcmd->flag) < 0) {
                error("cannot open %s\n", rcmd->path);
                return;
            }
            exec_cmd(rcmd->cmd);
            break;

        case PIPE:
            pcmd = cmd->ncmd;
            if(pipe(pi) < 0) {
                error("pipe failed\n");
                return;
            }
            if(safe_fork() == 0) {
                close(1);
                dup(pi[1]);
                close(pi[0]);
                close(pi[1]);
                exec_cmd(pcmd->lcmd);
            }
            if(safe_fork() == 0) {
                close(0);
                dup(pi[0]);
                close(pi[0]);
                close(pi[1]);
                exec_cmd(pcmd->rcmd);
            }
            close(pi[0]);
            close(pi[1]);
            wait(0);
            wait(0);
            break;

        case BACK:
            bcmd = cmd->ncmd;
            if(fork() == 0) {
                run_cmd(bcmd->cmd);
            }
            // do not wait
            break;

        case EXEC:
            exec_cmd(cmd->ncmd);
        default:
            break;
    }
}


int main(int argc)
{
    // no user argc here
    int fd;
    // open in out error
    while ((fd = open("console", O_RDWR)) < 3) {;}
    close(fd);
    
    for(;;) {
        showpwd();
        fprintf(2, "# ");
        memset(shbuf, 0, sizeof(shbuf));
        gets(shbuf, sizeof(shbuf));
        if(shbuf[0] == '\n' || shbuf[0] == 0) {
            continue;
        }
        if(shbuf[0]=='c' && shbuf[1]=='d' && shbuf[2]==' ') {
            cd();
            continue;
        }
        if(safe_fork() == 0) {
            struct cmd *cmd = parse_cmd();
            run_cmd(cmd);
            exit(0);
        }
        wait(0);
        
    }
    exit(0);
}