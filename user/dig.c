#include "./lib/user.h"
#include "../kernel/net/net.h"

int main(int argc, char *argv[])
{
    if(argc == 1) {
        error("usage: dig [domain name]\n");
    } else if(argc > 2) {
        error("dig: too many arguments\n");
    } else {
        struct dns_result dns_rs[10];
        memset(dns_rs, 0, sizeof(dns_rs));

        dns(argv[1], dns_rs);

        for(int i=0; i<10; i++) {
            if(dns_rs[i].ip != 0) {
                uint32 ip = dns_rs[i].ip;
                uint32 ttl = dns_rs[i].ttl;
                uint16 type = dns_rs[i].type;
                printf("%s\t%d\t%d\t%d.%d.%d.%d\n", argv[1], ttl, type, ip>>24&0xFF, ip>>16&0xFF, ip>>8&0xFF, ip&0xFF);
            } else {
                break;
            }
        }
        
    }
    exit(0);
}
