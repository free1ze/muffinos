#ifndef _NET_H
#define _NET_H

#include "../lock.h"

#define mov_p(p, n) (typeof(p))(((char *)(p) + (n)))

// buffer
#define DEFAULT_HEAD_LEN 500
#define MAX_DATA_LEN  1000

struct mbuf
{
    struct mbuf *next;
    int len;
    char *head;
    char data[DEFAULT_HEAD_LEN + MAX_DATA_LEN];
};
// 
// [--------HEADER|------DATA-----]
//        -> rhead
//        <- whead
//                 -> wdata
struct mbufq
{
    struct mbuf *head;
    struct mbuf *tail;
};

static inline uint16 bswaps(uint16 val)
{
  return (((val & 0x00ffU) << 8) |
          ((val & 0xff00U) >> 8));
}

static inline uint32 bswapl(uint32 val)
{
  return (((val & 0x000000ffUL) << 24) |
          ((val & 0x0000ff00UL) << 8) |
          ((val & 0x00ff0000UL) >> 8) |
          ((val & 0xff000000UL) >> 24));
}

// static inline int foo(int a)
// {
//   return 1;
// }

#define ntohs bswaps
#define ntohl bswapl
#define htons bswaps
#define htonl bswapl

// ETH
#define ETHTYPE_IP  0x0800 // Internet protocol
#define ETHTYPE_ARP 0x0806 // Address resolution protocol
struct eth
{
    uint8   dst[6];
    uint8   src[6];
    uint16  type;
}__attribute__((packed));


// IP
#define IPPROTO_ICMP 1  // Control message protocol
#define IPPROTO_TCP  6  // Transmission control protocol
#define IPPROTO_UDP  17 // User datagram protocol
struct ip
{
    uint8   vhl;    // version << 4 | header len
    uint8   tos;    // type of service
    uint16  len;    // total len
    uint16  id;     // identification
    uint16  off;    // flag << 14 | segment offset
    uint8   ttl;    // time to live
    uint8   prot;   // protocal
    uint16  sum;    // header check sum
    uint32  src_ip; // source ip
    uint32  dst_ip; // destination ip
}__attribute__((packed));

#define IP_ADDR(a, b, c, d) \
  (((uint32)a << 24) | ((uint32)b << 16) | \
   ((uint32)c << 8) | (uint32)d)

// UDP
struct udp
{
    uint16  src_p;  // source port
    uint16  dst_p;  // destination port
    uint16  len;    // total len
    uint16  sum;    // check sum
}__attribute__((packed));

// TCP
struct tcp
{
    uint16  src_p;  // source port
    uint16  dst_p;  // destination port
    uint32  seq;    // sequence number
    uint32  ack;    // acknowledge number
    uint8   hl;     // header len << 4
    uint8   flags;  // CWR << 7 | ECN << 6 | URG << 5 | ACK << 4 | PSH << 3 | RST << 2 | SYN << 1 | FIN
    uint16  wsz;    // window size
    uint16  sum;    // check sum
    uint16  up;     // urgent pointer
    uint32  opt;    // options
}__attribute__((packed));

// ARP
struct arp
{
    uint16  htype;  // hardware type                1
    uint16  ptype;  // protocal type                0x0800
    uint8   hlen;   // hardware address length      6
    uint8   plen;   // protocal address length      4
    uint16  op;     // operation type, 1 for request, 2 for reply
    uint8   src_m[6];   // source mac address
    uint32  src_ip; //  source ip address
    uint8   dst_m[6];   // destination mac address
    uint32  dst_ip; // destination ip address
}__attribute__((packed));

#define ARP_HRD_ETHER   1 // Ethernet

#define ARP_OP_REQUEST  1 // requests hw addr given protocol addr
#define ARP_OP_REPLY    2   // replies a hw addr given protocol addr

// an DNS packet (comes after an UDP header).
struct dns {
  uint16 id;  // request ID

  uint8 rd: 1;  // recursion desired
  uint8 tc: 1;  // truncated
  uint8 aa: 1;  // authoritive
  uint8 opcode: 4; 
  uint8 qr: 1;  // query/response
  uint8 rcode: 4; // response code
  uint8 cd: 1;  // checking disabled
  uint8 ad: 1;  // authenticated data
  uint8 z:  1;  
  uint8 ra: 1;  // recursion available
  
  uint16 qdcount; // number of question entries
  uint16 ancount; // number of resource records in answer section
  uint16 nscount; // number of NS resource records in authority section
  uint16 arcount; // number of resource records in additional records
} __attribute__((packed));

struct dns_question {
  uint16 qtype;
  uint16 qclass;
} __attribute__((packed));
  
#define ARECORD (0x0001)
#define QCLASS  (0x0001)

struct dns_data {
  uint16 type;
  uint16 class;
  uint32 ttl;
  uint16 len;
} __attribute__((packed));

// for user level to store dns result
struct dns_result {
  uint32 ttl;
  uint32 ip;
  uint16 type;
}__attribute__((packed));

#define ICMP_PING 8

struct icmp {
  uint8   type;
  uint8   code;
  uint16  checksum;
  uint16  id;
  uint16  seq;
  char    data[56];
} __attribute__((packed));

#endif