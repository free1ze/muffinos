#include "../fs/file.h"
#include "../types.h"
#include "../defs.h"
#include "../proc.h"
#include "../memlayout.h"
#include "socket.h"

struct socket *sock_list;
struct spinlock sock_list_lk;

void init_socket()
{
    init_spinlock(&sock_list_lk, "socket list lock");
    sock_list = NULL;
    log("socket init complete\n");
}

struct socket *sock_list_find(uint32 dst_ip, uint16 dst_p, uint16 src_p, uint16 type)
{
    struct socket *s = sock_list;
    while (s)
    {
        if(dst_ip == s->dst_ip && 
            dst_p == s->dst_p && 
            src_p == s->src_p && 
            type == s->type ) {
                break;
        }
        s = s->next;
    }
    return s;
}

// fail if socket already in list
int sock_list_add(struct socket *s)
{
    lock_spin(&sock_list_lk);
    if( sock_list_find(s->dst_ip, s->dst_p, s->src_p, s->type) ) {
        unlock_spin(&sock_list_lk);
        return -1;
    }
    s->next = sock_list;
    sock_list = s;
    unlock_spin(&sock_list_lk);
    return 0;
}

int sock_list_delete(struct socket *s)
{
    lock_spin(&sock_list_lk);
    struct socket **now = &sock_list;
    while (*now)
    {
        if(*now == s) {
            *now = s->next;
            break;
        }
        now = &((*now)->next);
    }
    unlock_spin(&sock_list_lk);
    return 0;
}

int socketalloc(struct file **f, uint32 dst_ip, uint16 dst_p, uint16 src_p, uint16 type)
{
    if(((*f) = filealloc()) == NULL) {
        return -1;
    }
    struct socket *sock  = (struct socket *)kalloc();
    sock->dst_ip = dst_ip;
    sock->src_ip = 0;
    sock->dst_p = dst_p;
    sock->src_p = src_p;
    sock->type = type;

    init_mbufq(&(sock->q));
    init_spinlock(&sock->lk, "socket lock");

    (*f)->type = FD_SOCK;
    (*f)->readable = true;
    (*f)->writable = true;
    (*f)->sock = sock;

    if( sock_list_add(sock) < 0 ) {
        kfree((uint64)sock);
        fileclose(*f);
    }
    return 0;
}

int socketread(struct socket *s, uint64 addr, int n)
{
    struct proc *p = myproc();

    lock_spin(&s->lk);
    while (mbufq_empty(&s->q) && !p->killed) {
        sleep(&s->q, &s->lk);
    }
    if (p->killed) {
        unlock_spin(&s->lk);
        return -1;
    }

    struct mbuf *m = mbufq_popfront(&s->q);
    unlock_spin(&s->lk);
    int len = min(n, m->len);
    int r = copyout(p->pagetable, (uint64)m->head, addr, len);
    mbuffree(m);

    return r;
}

int socketwrite(struct socket *s, uint64 addr, int n)
{
    struct proc *p = myproc();
    struct mbuf *m = mbufalloc();
    if( m == NULL ) {
        return -1;
    }
    if( n > MAX_DATA_LEN ) {
        return -1;
    }

    uint64 dst = (uint64)mbuf_wdata(m, n);
    int r = copyin(p->pagetable, addr, dst, n);
    
    if(s->type == IPPROTO_UDP) {
        send_udp(m, s->dst_ip, s->dst_p, s->src_p);
    } else if(s->type == IPPROTO_ICMP){
        send_ip(m, IPPROTO_ICMP, s->dst_ip);
    } else {
        error("unknown IP protocal");
    }
    return r;
}

int socketclose(struct socket *s)
{
    // log("socket close\n");
    if( s == NULL ){
        return 0;
    }

    sock_list_delete(s);

    lock_spin(&s->lk);
    while (!mbufq_empty(&s->q))
    {
        struct mbuf *m = mbufq_popfront(&s->q);
        mbuffree(m);
    }
    unlock_spin(&s->lk);
    kfree((uint64)s);
    return 0;
}

void push_packet(struct mbuf *m, uint32 dst_ip, uint16 dst_p, uint16 src_p, uint16 type)
{
    lock_spin(&sock_list_lk);
    struct socket *s = sock_list_find(dst_ip, dst_p, src_p, type);

    if(s) {
        lock_spin(&s->lk);
        mbufq_pushback(&s->q, m);
        wakeup(&s->q);
        unlock_spin(&s->lk);
    }  else {
        // error("fail to push!");
        mbuffree(m);
    }
    unlock_spin(&sock_list_lk);
}

int 
sys_connect()
{
    int dst_ip, dst_p, src_p, type;

    if(arg_int(0, &dst_ip) < 0 ||
        arg_int(1, &dst_p) < 0 ||
        arg_int(2, &src_p) < 0 ||
        arg_int(3, &type) < 0 ) {
        return -1;
    }
    struct file *f;
    if( socketalloc(&f, dst_ip, dst_p, src_p, type) < 0) {
        return -1;
    }
    int fd;
    if((fd = fdalloc(f)) < 0){
        fileclose(f);
        return -1;
    }

    return fd;
}