#include "../types.h"
#include "../param.h"
#include "../memlayout.h"
#include "../riscv.h"
#include "../lock.h"
#include "../proc.h"
#include "../defs.h"
#include "net.h"

static uint32 local_ip = IP_ADDR(10, 0, 2, 15); // qemu's idea of the guest IP
static uint8 local_mac[6] = { 0x52, 0x54, 0x00, 0x12, 0x34, 0x56 };
static uint8 broadcast_mac[6] = { 0xFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF };

void init_mbufq(struct mbufq *q)
{
    q->head = q->tail = NULL;
}

void mbufq_pushback(struct mbufq *q, struct mbuf *m) 
{
    if(q->tail == NULL) {
        q->head = q->tail = m;
    } else {
        q->tail->next = m;
    }
    m->next = NULL;
}

struct mbuf *mbufq_popfront(struct mbufq *q) 
{
    struct mbuf *ret = NULL;
    if(q->head == NULL) {
        ret = NULL;
    } else if(q->head == q->tail){
        ret = q->head;
        q->head = q->tail = NULL;
    } else {
        ret = q->head;
        q->head = q->head->next;
    }
    return ret;
}

int mbufq_empty(struct mbufq *q)
{
    if(q == NULL) {
        return true;
    }
    return (q->head == NULL);
}

uint16 checksum(void *head, int len)
{
    char *p = head;
    uint32 sum = 0;
    for(int i=0; i<len; i+=2,p+=2) {
        sum += (*p << 8) + (*(p+1));
    }
    if(len % 2 != 0) {
        char *last = ((char *)head) + len;
        sum += (*last << 8);
    }
    uint32 res = (sum >> 16) + (sum & 0x0000FFFF);
    uint16 checksum = ~res;
    return checksum;
}

struct mbuf *mbufalloc()
{
    struct mbuf *m = (struct mbuf *)kalloc();
    m->head = m->data + DEFAULT_HEAD_LEN;
    m->len = 0;
    m->next = 0;
    memset(m->data, 0 , sizeof(m->data));
    return m;
}

void mbuffree(struct mbuf *m)
{
    kfree((uint64)m);
}

char *mbuf_rhead(struct mbuf *m, int len)
{
    assert(m != NULL);
    assert(m->len >= len);

    char *tmp = m->head;
    m->head += len;
    m->len -= len;
    return tmp;
}

char *mbuf_whead(struct mbuf *m, int len)
{
    assert(m != NULL);
    
    m->head -= len;
    m->len += len;
    return m->head;
}

// char *mbuf_rdata(struct mbuf *m, int len)
// {
//     assert(m != NULL);

//     char *tmp = m->head + m->len;
//     m->len -= len;
//     return tmp;
// }

char *mbuf_wdata(struct mbuf *m, int len)
{
    assert(m != NULL);
    
    char *tmp = m->head + m->len;
    m->len += len;
    return tmp;
}

/* send packet */
void send_eth(struct mbuf *m, uint16 type)
{
    // log("send eth\n");

    struct eth *ehdr = (struct eth *)mbuf_whead(m, sizeof(*ehdr));
    memcpy(ehdr->dst, broadcast_mac, 6);
    memcpy(ehdr->src, local_mac, 6);
    ehdr->type = htons(type);

    if(e1000_transmit(m)) {
        mbuffree(m);
    }
}

void send_ip(struct mbuf *m, uint8 type, uint32 dst_ip)
{
    // log("send ip\n");

    struct ip *iphdr = (struct ip *)mbuf_whead(m, sizeof(*iphdr));
    memset(iphdr, 0, sizeof(*iphdr));

    iphdr->vhl = (4 << 4) | (20 >> 2);
    iphdr->prot = type;
    iphdr->dst_ip = htonl(dst_ip);
    iphdr->src_ip = htonl(local_ip);
    iphdr->len = htons(m->len);
    iphdr->ttl = 100;
    iphdr->sum = htons(checksum(iphdr, sizeof(*iphdr)));

    send_eth(m, ETHTYPE_IP);
}

void send_udp(struct mbuf *m, uint32 dst_ip, uint16 dst_p, uint16 src_p)
{
    // log("send udp\n");

    struct udp *udphdr = (struct udp *)mbuf_whead(m, sizeof(*udphdr));
    memset(udphdr, 0, sizeof(*udphdr));

    udphdr->src_p = htons(src_p);
    udphdr->dst_p = htons(dst_p);
    udphdr->len = htons(m->len);
    // udphdr->sum = ltons(checksum(udphdr, sizeof(*udphdr)));
    udphdr->sum = 0;        // disable checksum

    send_ip(m, IPPROTO_UDP, dst_ip);
}

void send_arp(uint16 op, uint32 dst_ip, uint8 dst_mac[6])
{
    // log("send arp\n");
    struct mbuf *m = mbufalloc();

    struct arp *arphdr = (struct arp *)mbuf_whead(m, sizeof(*arphdr));
    memset(arphdr, 0, sizeof(*arphdr));

    arphdr->htype = htons(ARP_HRD_ETHER);
    arphdr->ptype = htons(ETHTYPE_IP);
    arphdr->hlen = 6;
    arphdr->plen = 4;
    arphdr->op = htons(op);
    memcpy(arphdr->src_m, local_mac, 6);
    arphdr->src_ip = htonl(local_ip);
    memcpy(arphdr->dst_m, dst_mac, 6);
    arphdr->dst_ip = htonl(dst_ip);

    send_eth(m, ETHTYPE_ARP);
}

/* receive packet */

void recv_icmp(struct mbuf *m, struct ip *iphdr, uint8 ttl)
{
    // log("recv_icmp\n",);

    uint32 dst_ip = ntohl(iphdr->src_ip);
    uint8 *data =(uint8 *)mov_p(m->head, 8);
    // save ttl in payload of ICMP packet
    *data = ttl;
    push_packet(m, dst_ip, 0, 0, IPPROTO_ICMP);
    return ;
}

void recv_arp(struct mbuf *m)
{
    // log("recv arp\n");

    struct arp *arphdr = (struct arp *)mbuf_rhead(m, sizeof(*arphdr));
    if (!arphdr) goto done;
    if(arphdr->htype != ntohs(ARP_HRD_ETHER)) goto done;
    if(arphdr->ptype != ntohs(ETHTYPE_IP)) goto done;
    if(arphdr->hlen != 6) goto done;
    if(arphdr->plen != 4) goto done;
    if(ntohs(arphdr->op) != ARP_OP_REQUEST) goto done;
    if(ntohl(arphdr->dst_ip) != local_ip) goto done; 

    uint32 dst_ip = ntohl(arphdr->src_ip);
    uint8 dst_mac[6];
    memcpy(dst_mac, arphdr->src_m, 6);
    send_arp(ARP_OP_REPLY, dst_ip, dst_mac);

    done:
        mbuffree(m);
}

void recv_tcp()
{
    error("unimplemented");
}

void recv_udp(struct mbuf *m, struct ip *iphdr, int len)
{
    // log("recv udp\n");
    struct udp *udphdr = (struct udp *)mbuf_rhead(m, sizeof(*udphdr));
    if (!udphdr)
        goto fail;

    // validate lengths reported in headers
    if(ntohs(udphdr->len) != len)
        goto fail;
    len -= sizeof(*udphdr);
    if (len > m->len)
        goto fail;

    // checksum disabled
    // if(checksum(udphdr, sizeof(*udphdr)) != 0)
    //     goto fail;

    uint32 dst_ip = ntohl(iphdr->src_ip);
    uint16 dst_p = ntohs(udphdr->src_p);
    uint16 src_p = ntohs(udphdr->dst_p);
    push_packet(m, dst_ip, dst_p, src_p, IPPROTO_UDP);
    return ;

    fail:
        mbuffree(m);
}

void recv_ip(struct mbuf *m)
{
    // log("recv ip\n");

    struct ip *iphdr = (struct ip *)mbuf_rhead(m, sizeof(*iphdr));

    if (!iphdr)
	    goto fail;

    // check IP version and header len
    if (iphdr->vhl != ((4 << 4) | (20 >> 2)))
        goto fail;
    // validate IP checksum
    if ( checksum(iphdr, sizeof(*iphdr)) != 0)
        goto fail;
    // can't support fragmented IP packets
    if (htons(iphdr->off) != 0)
        goto fail;
    // is the packet addressed to us?
    if (htonl(iphdr->dst_ip) != local_ip)
        goto fail;

    int len;
    switch (iphdr->prot)
    {
    case IPPROTO_UDP:
        len = ntohs(iphdr->len) - sizeof(*iphdr);
        recv_udp(m, iphdr, len);
        break;
    case IPPROTO_TCP:
        recv_tcp();
        break;
    case IPPROTO_ICMP:
        recv_icmp(m, iphdr, iphdr->ttl);
        break;

    default:
        goto fail;
        break;
    }
    return;

    fail:
        mbuffree(m);
}
void recv_eth(struct mbuf *m)
{
    // log("recv eth\n");

    struct eth *ehdr = (struct eth *)mbuf_rhead(m, sizeof(*ehdr));

    // char dst[6];
    // char src[6];
    // memcpy(dst, ehdr->dst, 6);
    // memcpy(src, ehdr->src, 6);
    uint16 type = ntohs(ehdr->type);

    switch (type)
    {
    case ETHTYPE_ARP:
        recv_arp(m);
        break;
    case ETHTYPE_IP:
        recv_ip(m);
        break;
    
    default:
        error("unknown eth type");
        mbuffree(m);
        break;
    }
}

// receive packet 
void recv_pkt(struct mbuf *m)
{
    // log("recv_pkt\n");
    if(m == NULL) {
        return;
    }
    recv_eth(m);
}