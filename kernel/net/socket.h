
#ifndef _SOCKET_H
#define _SOCKET_H

#include "../types.h"
#include "net.h"

#define AF_INET 1

#define SOCK_STREAM     1
#define SOCK_DGRAM      2

struct socket 
{
    struct socket *next;
    uint16 type;
    uint32 src_ip;
    uint16 src_p;
    uint32 dst_ip;
    uint16 dst_p;
    struct spinlock lk;
    struct mbufq q;
};

#endif