#ifndef _PARAM_H
#define _PARAM_H

#define NCPU            1
#define NPROC           10
#define NBUF            32
#define MAX_INT_LEN     65
#define BSIZE           1024
#define NOFILE          16  // open files per process
#define NFILE           100
#define MAXPATH         128
#define MAXARG          32  // max exec arguments
#define FSSIZE          1000  // size of file system in blocks
#define NDEV            10

#endif