#include "defs.h"
#include "param.h"
#include "types.h"
#include "lock.h"
#include "proc.h"
#include "fs/file.h"


#define PIPESIZE    512
struct pipe {
    char data[PIPESIZE];
    uint64 w;
    uint64 r;

    int writeopen;
    int readopen;
    struct spinlock lock;
};

int pipealloc(struct file **f0, struct file **f1)
{
    struct pipe *pp = (struct pipe *)kalloc();
    *f0 = *f1 = 0;

    if( ((*f0 = filealloc()) == 0) || ((*f1 = filealloc()) == 0)) {
        goto fail;
    }
    init_spinlock(&pp->lock, "pipe lock");
    pp->w = pp->r = 0;
    pp->readopen = pp->writeopen = true;

    (*f0)->writable = false;
    (*f0)->readable = true;
    (*f0)->type = FD_PIPE;
    (*f0)->pipe = pp;
    (*f1)->writable = true;
    (*f1)->readable = false;
    (*f1)->type = FD_PIPE;
    (*f1)->pipe = pp;
    return 0;

    fail:
    if(pp) {
        kfree((uint64)pp);
    }
    if(*f0) {
        fileclose(*f0);
    }
    if(*f1) {
        fileclose(*f1);
    }
    return -1;
}

void pipeclose(struct pipe *pp, int iswrite)
{
    lock_spin(&pp->lock);
    if(iswrite) {
        pp->writeopen = false;
        wakeup(&pp->r);
    } else {
        pp->readopen = false;
        wakeup(&pp->w);
    }
    if(!pp->writeopen && !pp->readopen) {
        unlock_spin(&pp->lock);
        kfree((uint64)pp);
    } else {
        unlock_spin(&pp->lock);
    }
}

int pipewrite(struct pipe *pp, uint64 addr, int n)
{
    if(!pp->writeopen) {
        return -1;
    }

    struct proc *p = myproc();
    lock_spin(&pp->lock);
    int i = 0;
    while(i < n) {
        if(pp->readopen == 0 || p->killed){
            unlock_spin(&pp->lock);
            return -1;
        }
        if(pp->r + PIPESIZE == pp->w) {
            wakeup(&pp->r);
            sleep(&pp->w, &pp->lock);
        } else {
            char ch;
            copyin(p->pagetable, addr+i, (uint64)&ch, 1);
            pp->data[pp->w++ % PIPESIZE] = ch;
            i++;
        }
    }
    wakeup(&pp->r);
    unlock_spin(&pp->lock);
    return i;
}


int piperead(struct pipe *pp, uint64 addr, int n)
{
    if(!pp->readopen) {
        return -1;
    }

    struct proc *p = myproc();
    lock_spin(&pp->lock);
    while (pp->r == pp->w && pp->writeopen)
    {
        if(p->killed) {
            unlock_spin(&pp->lock);
            return -1;
        }
        sleep(&pp->r, &pp->lock);
    }
    
    int i;
    for(i=0; i<n; i++) {
        if(pp->r == pp->w) {
            break;
        }
        char ch = pp->data[pp->r++ % PIPESIZE];
        if( copyout(p->pagetable, (uint64)&ch, addr+i, 1) == -1 ) {
            break;
        }
    }
    wakeup(&pp->w);
    unlock_spin(&pp->lock);
    return i;
}