#include "types.h"
#include "defs.h"
#include "lock.h"
#include "proc.h"

struct spinlock cons_lock;
#define CONS_BUF_SIZE 128
char cons_buf[CONS_BUF_SIZE];
uint64 cons_end;
uint64 cons_r;
uint64 cons_w;

#define BACKSPACE 0x100
#define C(x)  ((x)-'@')  // Control-x


void init_console()
{
    init_uart();
    init_spinlock(&cons_lock, "console lock");
    log("uart/console init complete\n");
}

void consputc(int c)
{
  if(c == BACKSPACE){
    uartputc_sync('\b'); uartputc_sync(' '); uartputc_sync('\b');
  } else {
    uartputc_sync(c);
  }
}


// copy out a whole line or at most n byte
int consoleread(uint64 dst, int n)
{
    lock_spin(&cons_lock);

    int i=0;
    while (i<n)
    {
        while(cons_r == cons_end) {
            sleep(&cons_r, &cons_lock);
        }
        char c = cons_buf[cons_r++ % CONS_BUF_SIZE];
        if( copyout(myproc()->pagetable, (uint64)&c, dst+i, 1) == -1 ) {
            break;
        }
        i++;
        if(c == '\n'){
            break;
        }
    }

    unlock_spin(&cons_lock);
    return i;
}

// from user space
int consolewrite(uint64 src, int n)
{
    char c;
    int i;
    for(i=0; i<n; i++){
        if( copyin(myproc()->pagetable, src + i, (uint64)&c, 1) != -1) {
            uartputc(c);
        } else {
            break;
        }
    }
    return i;
}

void console_intr(int c)
{
    lock_spin(&cons_lock);

    switch (c)
    {
    // backspace
    case C('H'):
    case 127:
        if(cons_w != cons_end) {
            cons_w--;
            consputc(BACKSPACE);
        }
        break;

    default:
        if(c == '\r') {
            c = '\n';
        }
        if(c == '\t') {
            c = ' ';
        }
        consputc(c);
        cons_buf[cons_w++ % CONS_BUF_SIZE] = c;

        // end of line
        if(c == '\n' || cons_w - cons_r == CONS_BUF_SIZE) {
            cons_end = cons_w;
            wakeup(&cons_r);
        }

        break;
    }
    unlock_spin(&cons_lock);
}