#include "defs.h"
#include "riscv.h"
#include "proc.h"
#include "memlayout.h"
#include "lock.h"

extern void kernelvec();
extern char trampoline[], uservec[], userret[];


#define EnvCallUmode        8
#define LoadPageFault       13
#define StorePageFault      15

struct spinlock ticklk;
uint64 tick;

void init_trap()
{
    init_spinlock(&ticklk, "tick lock");
    tick = 0;
    log("trap init complete\n");
}

void init_traphart()
{
    w_stvec((uint64)kernelvec);
}

void clock_intr()
{
    lock_spin(&ticklk);
    tick++;
    wakeup(&tick);
    unlock_spin(&ticklk);
}

uint64 time()
{
    lock_spin(&ticklk);
    uint64 time = tick; 
    unlock_spin(&ticklk);
    return time;
}


// check if it's an external interrupt or software interrupt,
// and handle it.
// returns 2 if timer interrupt,
// 1 if other device,
// 0 if not recognized.
int dev_intr()
{
  uint64 scause = r_scause();

  if((scause & 0x8000000000000000L) &&
     (scause & 0xff) == 9){
    // this is a supervisor external interrupt, via PLIC.

    // irq indicates which device interrupted.
    int irq = plic_claim();

    if(irq == UART0_IRQ){
      uart_intr();
    } else if(irq == VIRTIO0_IRQ){
      virtio_disk_intr();
    } else if(irq == E1000_IRQ){
      e1000_intr();
    } else if(irq){
      printf("unexpected interrupt irq=%d\n", irq);
    }

    // the PLIC allows each device to raise at most one
    // interrupt at a time; tell the PLIC the device is
    // now allowed to interrupt again.
    if(irq)
      plic_complete(irq);

    return 1;
  } else if(scause == 0x8000000000000001L){
    // software interrupt from a machine-mode timer interrupt,
    // forwarded by timervec in kernelvec.S.

    if(cpuid() == 0){
      clock_intr();
    }
    
    // acknowledge the software interrupt by clearing
    // the SSIP bit in sip.
    w_sip(r_sip() & ~2);

    return 2;
  } else {
    return 0;
  }
}

void kerneltrap()
{
  // printf("switch to kernel trap handler\n");
  int dev = 0;
  uint64 sepc = r_sepc();
  uint64 sstatus = r_sstatus();
  uint64 scause = r_scause();
  
  if((sstatus & SSTATUS_SPP) == 0)
    error("kerneltrap: not from supervisor mode");
  if(intr_get() != 0)
    error("kerneltrap: interrupts enabled");

  if((dev = dev_intr()) == 0){
    printf("scause %p\n", scause);
    printf("sepc = %p stval = %p\n", r_sepc(), r_stval());
    error("kerneltrap");
  }

  // give up the CPU if this is a timer interrupt.
  if(dev == 2 && myproc() != 0 && myproc()->state == RUNNING)
    yield();

  // the yield() may have caused some traps to occur,
  // so restore trap registers for use by kernelvec.S's sepc instruction.
  w_sepc(sepc);
  w_sstatus(sstatus);
}

void usertrap()
{
    if((r_sstatus() & SSTATUS_SPP) != 0){
      error("usertrap: not from user mode");
    }

    // delegate traps to kerneltrap 
    w_stvec((uint64)kernelvec);
    uint64 sepc = r_sepc();

    struct proc *p = myproc();
    // uint64 epc = p->trapframe->epc;
    uint64 scause = r_scause();
    int dev = 0;
    if(scause == EnvCallUmode) {
        if(p->killed) {
            goto kill;
        }
        p->trapframe->epc = sepc + 4;

        intr_on();
        syscall();
    } else if( (dev = dev_intr()) != 0) {
        // ok
    } else {
        printf("usertrap(): unexpected scause %p pid=%d\n", scause, p->pid);
        printf("            sepc=%p stval=%p\n", r_sepc(), r_stval());
        error("usertrap");
        goto kill;
    }

    if(dev == 2) {
        yield();
    }

    // no return
    usertrapret();

    error("usertrap return");

    kill:
    exit(-1);
}


// set up sstatus and sepc.
void usertrapret()
{
    struct proc* p = myproc();

    intr_off();

    // send trap to uservec
    w_stvec(TRAMPOLINE + (uservec - trampoline));

    p->trapframe->kernel_satp = r_satp();         // kernel page table

    p->trapframe->kernel_sp = p->kstack + PGSIZE; // process's kernel stack

    p->trapframe->kernel_trap = (uint64)usertrap;
    p->trapframe->kernel_hartid = r_tp();         // hartid for cpuid()

    // set S Previous Privilege mode to User.
    unsigned long x = r_sstatus();
    x &= ~SSTATUS_SPP; // clear SPP to 0 for user mode
    x |= SSTATUS_SPIE; // enable interrupts in user mode
    w_sstatus(x);

    // set S Exception Program Counter to the saved user pc.
    w_sepc(p->trapframe->epc);

    // tell trampoline.S the user page table to switch to.
    uint64 satp = MAKE_SATP(p->pagetable);

    uint64 fn = TRAMPOLINE + (userret - trampoline);
    ((void (*)(uint64,uint64))fn)(TRAPFRAME, satp);
}
