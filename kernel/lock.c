#include "lock.h"
#include "defs.h"
#include "proc.h"
#include "riscv.h"

// turn off interupts with counter
void push_off()
{
    struct cpu* c = mycpu();
    int old = intr_get();
    intr_off();
    assert(c->noff >= 0);
    if(c->noff == 0) {
        c->intena = old;
    }
    c->noff++;
}

void pop_off()
{
    struct cpu* c = mycpu();
    if(intr_get()) {
        error("pop_off: should not be interruptible");
    }
    if(c->noff < 1) {
        error("pop_off");
    }
    
    c->noff--;
    if(c->noff == 0 && c->intena){
        intr_on();
    }
}

// spinlock
void init_spinlock(struct spinlock *lk, char *name)
{
    lk->locked = false;
    lk->cpu = mycpu();
    lk->name = name;
}

inline
int holding_spin(struct spinlock *lk)
{
    return lk->locked && lk->cpu == mycpu();
}

void lock_spin(struct spinlock *lk)
{
    push_off();

    if(holding_spin(lk)) {
        error("cpu %d should not hold spin '%s'\n",cpuid(), lk->name);
    }

    while(__sync_lock_test_and_set(&lk->locked, 1) != 0)
        ;
    __sync_synchronize();

    lk->cpu = mycpu();
    // if(strcmp("ftable lock", lk->name) == 0 ) {
    //     log("lock %s\n", lk->name);
    // }
}

void unlock_spin(struct spinlock *lk)
{
    if(!holding_spin(lk)) {
        error("cpu %d should hold spin '%s'\n",cpuid(), lk->name);
    }

    lk->cpu = NULL;

    __sync_synchronize();
    __sync_lock_release(&lk->locked);

    pop_off();
    // if(strcmp("ftable lock", lk->name) == 0 ) {
    //     log("unlock %s\n", lk->name);
    // }
}

// sleeplock
void init_sleeplock(struct sleeplock *lk, char *name)
{
    lk->locked = false;
    lk->pid = NULL;
    lk->name = name;
    init_spinlock(&lk->lock, name);
}

inline
int holding_sleep(struct sleeplock *lk)
{
    lock_spin(&lk->lock);
    int holding = (lk->locked && lk->pid == myproc()->pid);
    unlock_spin(&lk->lock);
    return holding;
}

void lock_sleep(struct sleeplock *lk)
{
    if(holding_sleep(lk)) {
        error("should not hold sleep '%s'\n", lk->name);
    }

    lock_spin(&lk->lock);
    while (lk->locked) {
        sleep(lk, &lk->lock);
    }
    lk->locked = true;
    lk->pid = myproc()->pid;
    unlock_spin(&lk->lock);
}

void unlock_sleep(struct sleeplock *lk)
{
    if(!holding_sleep(lk)) {
        error("should hold sleep '%s'\n", lk->name);
    }

    lock_spin(&lk->lock);
    lk->locked = false;
    lk->pid = NULL;
    wakeup(lk);
    unlock_spin(&lk->lock);
}