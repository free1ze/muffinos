#include "types.h"
#include "defs.h"
#include "memlayout.h"
#include "proc.h"
#include "riscv.h"

extern char etext[];  // kernel.ld sets this to end of kernel code.

extern char trampoline[]; // trampoline.S

pagetable_t kernel_pagetable = 0;

void dump_page(pagetable_t pgtbl, uint64 va) {
    push_off();
    va = PGROUNDDOWN(va);
    uint64 addr = walkaddr(va, pgtbl);
    char *data = (char *)addr;
    uint64 satp =r_satp();
    w_satp(0);
    for(int i = 0; i < PGSIZE; i++) {
        printf("%p ",data[i]);
        if(i%16==15) {
            printf("\n");
        }
    }
    w_satp(satp);
    pop_off();
} 

// return top level pte pointer if found
// else alloc pte table (not physical page) if alloc flag set
// else error
pte_t *walk(uint64 va, pagetable_t pgtbl, int alloc)
{
    if(va >= MAXVA){
        error("walk");
    }

    for(int level = 2; level > 0; level--) {
        pte_t *pte = &pgtbl[VX(va, level)];
        if(*pte & PTE_V) {
            pgtbl = (pagetable_t)PTE2PA(*pte);
        } else {
            if(!alloc) {
                return 0;
            }
            pgtbl = (pte_t*)kalloc();
            *pte = PA2PTE(pgtbl) | PTE_V;
        }
    }
    return &pgtbl[VX(va, 0)];
}

// try fetching user space pa 
uint64 walkaddr(uint64 va, pagetable_t pgtbl)
{
    pte_t *pte = walk(va, pgtbl, false);
    if(! (*pte | PTE_V) ) {
        return 0;
    }
    if(! (*pte | PTE_U) ) {
        return 0;
    }
    return PTE2PA(*pte) + (va - PGROUNDDOWN(va));
}

// pa must been allocated before
// only does the mapping
int vmmap(uint64 va, uint64 pa, pagetable_t pgtbl, int size, uint64 permission)
{
    va = PGROUNDDOWN(va);
    pa = PGROUNDDOWN(pa);
    while (size > 0)
    {
        pte_t *pte = walk(va, pgtbl, true);
        if(*pte & PTE_V) {
            error("vmmap: remap at addr: pa%p, va:%p", pa, va);
        }
        *pte = MKPTE(pa) | permission | PTE_V;
        size -= PGSIZE;
        va += PGSIZE;
        pa += PGSIZE;
    }

    return 0;
}

int vmunmap(uint64 va, pagetable_t pgtbl, int size, int do_free)
{
    assert(va % PGSIZE == 0);
    while(size > 0) {
        pte_t *pte = walk(va, pgtbl, false);
        // assert ( *pte & PTE_U );
        assert ( *pte & PTE_V );
        if(do_free) {
            uint64 pa = PTE2PA(*pte);
            kfree(pa);
        }
        *pte = 0;

        size -= PGSIZE;
        va += PGSIZE;
    }

    return 0;
}

static
void free_pgtbl_level(pagetable_t pgtbl, int level)
{
    pte_t *ptes = (pte_t *)pgtbl;
    for(int i=0; i<512; i++) {
        pte_t *pte = (pte_t *)(&ptes[i]);
        if( *pte & PTE_V ) {
            uint64 pa = PTE2PA(*pte);
            if( level > 0 ) {
                free_pgtbl_level((pagetable_t)pa, level-1);
            } else {
                kfree(pa);
            }
        }
    }
    kfree((uint64)pgtbl);
}

void free_pgtbl(pagetable_t pgtbl)
{
    vmunmap(TRAMPOLINE, pgtbl, PGSIZE, false);
    vmunmap(TRAPFRAME, pgtbl, PGSIZE, false);
    free_pgtbl_level(pgtbl, 2);
}

static
void print_pgtbl_level(pagetable_t pgtbl, int level)
{
    pte_t *ptes = (pte_t *)pgtbl;
    for(int i=0; i<512; i++) {
        pte_t *pte = (pte_t *)(&ptes[i]);
        if( *pte & PTE_V ) {
            pagetable_t npgtbl = (pagetable_t)PTE2PA(*pte);
            if( level == 2  ) {
                printf(".. %d: pa %p\n", i, npgtbl);
                print_pgtbl_level(npgtbl, 1);
            } else if( level == 1 ){
                printf(".. ..%d: pa %p\n", i, npgtbl);
                print_pgtbl_level(npgtbl, 0);
            } else if( level == 0){
                printf(".. .. ..%d: pa %p\n", i, npgtbl);
            } else {
                error("wrong level");
            }
        }
    }
}

void dump_pgtbl(pagetable_t pgtbl)
{
    printf("pgtbl: %p\n", pgtbl);
    print_pgtbl_level(pgtbl, 2);
}

pagetable_t alloc_pagetable()
{
    return (pagetable_t)kalloc();
}

// alloc empty a pgtbl and trapframe, map TRAMPOLINE and trapframe 
pagetable_t make_uvm(struct proc *p)
{
    pagetable_t pgtbl = alloc_pagetable();
    vmmap(TRAMPOLINE, (uint64)trampoline, pgtbl, PGSIZE, PTE_R | PTE_X);

    p->trapframe = (trapframe_t)kalloc();
    vmmap(TRAPFRAME, (uint64)(p->trapframe), pgtbl, PGSIZE, PTE_R | PTE_W);

    return pgtbl;
}

void init_kvm()
{
    kernel_pagetable = alloc_pagetable();
    memset((char *)kernel_pagetable, 0, PGSIZE);

    // uart registers
    vmmap(UART0, UART0, kernel_pagetable, PGSIZE, PTE_R | PTE_W);

    // virtio mmio disk interface
    vmmap(VIRTIO0, VIRTIO0, kernel_pagetable, PGSIZE, PTE_R | PTE_W);

    // PLIC
    vmmap(PLIC, PLIC, kernel_pagetable, 0x400000, PTE_R | PTE_W);

    // map kernel text executable and read-only.
    vmmap(KERNBASE, KERNBASE, kernel_pagetable, (uint64)etext-KERNBASE, PTE_R | PTE_X);

    // map kernel data and the physical RAM we'll make use of.
    vmmap((uint64)etext, (uint64)etext, kernel_pagetable, PHYSTOP-(uint64)etext, PTE_R | PTE_W);

    // map the trampoline for trap entry/exit to
    // the highest virtual address in the kernel.
    vmmap(TRAMPOLINE, (uint64)trampoline, kernel_pagetable, PGSIZE, PTE_R | PTE_X);

    // PCI-E ECAM (configuration space), for pci.c
    vmmap(0x30000000L, 0x30000000L, kernel_pagetable, 0x10000000, PTE_R | PTE_W);

    // pci.c maps the e1000's registers here.
    vmmap(0x40000000L, 0x40000000L, kernel_pagetable, 0x20000, PTE_R | PTE_W);

    // map kernel stacks with an unmapped guard page below
    struct proc *p;
    for(p = proc; p < &proc[NPROC]; p++) {
        uint64 pa = kalloc();
        uint64 va = KSTACK((int) (p - proc));
        vmmap(va, pa, kernel_pagetable, PGSIZE, PTE_R | PTE_W);
    }

    log("kernel virturl memory init complete\n");
}

void init_kvmhart()
{
    // write satp and turn on vm
    w_satp(MAKE_SATP(kernel_pagetable));
    sfence_vma();
}

// from user to kernel
// return -1 on error
int copyin(pagetable_t pgtbl, uint64 src, uint64 dst, int len)
{
    int cc = len;
    while(len > 0) {
        uint64 srcpa = walkaddr(src, pgtbl);
        if(srcpa == 0) {
            return -1;
        }
        int nlen = NEXTPG(srcpa) - srcpa;
        nlen = min(nlen, len);
        memcpy((char *)dst, (char *)srcpa, nlen);
        dst = NEXTPG(dst);
        src = NEXTPG(src);
        len -= nlen;
    }
    return cc;
}

// from kernel to user
// return -1 on error
int copyout(pagetable_t pgtbl, uint64 src, uint64 dst, int len)
{
    int cc = len;
    while(len > 0) {
        uint64 dstpa = walkaddr(dst, pgtbl);
        if(dstpa == 0) {
            return -1;
        }
        int nlen = NEXTPG(dstpa) - dstpa;
        nlen = min(nlen, len);
        memcpy((char *)dstpa, (char *)src, nlen);
        dst = NEXTPG(dst);
        src = NEXTPG(src);
        len -= nlen;
    }

    return cc;
}

int copyinstr(pagetable_t pgtbl, uint64 src, uint64 dst, uint64 len)
{
    while(len > 0) {
        uint64 srcpa = walkaddr(src, pgtbl);
        if(srcpa == 0) {
            return -1;
        }
        int nlen = NEXTPG(srcpa) - srcpa;
        nlen = min(nlen, len);
        int i=0;
        while(i<nlen) {
            memcpy((char *)dst+i, (char *)srcpa+i, 1);
            char ch = *(char *)(srcpa+i);
            i++;
            if(ch == 0) {
                return 0;
            }
        }
        dst = NEXTPG(dst);
        src = NEXTPG(src);
        len -= nlen;
    }

    return len;
}

int either_copyout(int from_user, uint64 dst, uint64 src, uint64 len)
{
    struct proc *p = myproc();
    if(from_user) {
        return copyout(p->pagetable, src, dst, len);
    } else {
        memcpy((char *)dst, (char *)src, len);
        return 0;
    }
}

int either_copyin(int from_user, uint64 dst, uint64 src, uint64 len)
{
  struct proc *p = myproc();
  if(from_user){
    return copyin(p->pagetable, src, dst, len);
  } else {
    memcpy((char *)dst, (char *)src, len);
    return 0;
  }
}

int uvmalloc(pagetable_t pgtbl, int oldsz, int n)
{
    assert(n > 0);
    n = PGROUNDUP(n);
    int sz = n;
    while(sz > 0) {
        uint64 mem = kalloc();
        vmmap(oldsz, mem, pgtbl, PGSIZE, PTE_W|PTE_X|PTE_R|PTE_U);
        sz -= PGSIZE;
        oldsz += PGSIZE;
    }

    return oldsz;
}

int uvmdealloc(pagetable_t pgtbl, int oldsz, int n)
{
    n = PGROUNDUP(n);
    assert(n > 0 && oldsz - n >=0);
    vmunmap(oldsz-n, pgtbl, n, true);

    return oldsz - n;
}

// return 0 on success
// return -1 on error
int growproc(int n)
{
    struct proc* p = myproc();
    lock_spin(&p->lk);
    int sz = p->sz;
    
    if(n > 0) {
        sz = uvmalloc(p->pagetable, sz, n);
    } else if(n < 0) {
        sz = uvmdealloc(p->pagetable, sz, abs(n));
    }
    p->sz = sz;

    unlock_spin(&p->lk);
    return 0;
}

// mark a PTE invalid for user access.
// used by exec for the user stack guard page.
void uvmclear(pagetable_t pagetable, uint64 va)
{
  pte_t *pte;
  
  pte = walk(va, pagetable, false);
  if(pte == 0)
    error("uvmclear");
  *pte &= ~PTE_U;
}

int uvmcopy(pagetable_t old, pagetable_t new, int sz)
{
    for(int n=0; n < sz; n+=PGSIZE) {
        pte_t *pte = walk(n, old, false);
        assert(*pte & PTE_V);
        uint64 flag = PTE_FLAGS(*pte);
        uint64 addr = PTE2PA(*pte);
        uint64 mem = kalloc();
        memcpy((char *)mem, (char *)addr, PGSIZE);
        vmmap(n, mem, new, PGSIZE, flag);
    }
    return 0;
}