#include "defs.h"

void memcpy(void *dst, const void *src, int len)
{
    // assert((len >= 0), "memcpy");
    assert(len > 0);
    char *p = dst;
    const char *q = src;
    while( len-- > 0) {
        *(p++) = *(q++);
    }
}

void memset(void *dst, char val, int len)
{
    assert((len >= 0));
    char *d = dst;
    while( len-- > 0) {
        *(d++) = val;
    }
}

int strcmp(const char *p, const char *q) 
{
    while(*p && *q && (*p == *q)) {
        p++;
        q++;
    }
    return *p - *q;
}

// user need to take care of the space
char *strcpy(char *dst,const char *src)
{
    memcpy(dst, src, strlen(src));
    return dst;
}

char *strcat(char *dst, const char *src)
{
    int dlen = strlen(dst);
    int slen = strlen(src);
    for(int i=0; i<slen; i++) {
        dst[i+dlen] = src[i];
    }
    return dst;
}

int strlen(const char *str)
{
    int len = 0;
    while(*str++){
        len++;
    }
    return len+1;
}