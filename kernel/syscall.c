#include "types.h"
#include "defs.h"
#include "syscall.h"
#include "proc.h"

static uint64 arg_get(int idx)
{
    struct proc *p = myproc();
    switch (idx)
    {
    case 0:
        return p->trapframe->a0;
    case 1:
        return p->trapframe->a1;
    case 2:
        return p->trapframe->a2;
    case 3:
        return p->trapframe->a3;
    case 4:
        return p->trapframe->a4;
    case 5:
        return p->trapframe->a5;
    case 6:
        return p->trapframe->a6;
    
    default:
        error("arg_get");
        break;
    }
    return 0;
}

int arg_int(int idx, int *dst)
{
    *dst = arg_get(idx);
    return 0;
}

int arg_ptr(int idx, uint64 *dst)
{
    *dst = arg_get(idx);
    return 0;
}

int arg_str(int idx, char *buf, int len) {
    uint64 addr;
    arg_ptr(idx, &addr);
    struct proc *p = myproc();
    int err = copyinstr(p->pagetable, addr, (uint64)buf, len);
    if(err < 0)
        return err;
    return strlen(buf);
}

uint64 
sys_test()
{
    printf("syscall test\n");
    return 0;
}

uint64 
sys_sbrk()
{
    int n;
    arg_int(0, &n);
    int sz = myproc()->sz;
    if( growproc(n) < 0) {
        return -1;
    }
    return sz;
}


uint64
sys_exit(void)
{
  int n;
  if(arg_int(0, &n) < 0)
    return -1;
  exit(n);
  return 0;  // not reached
}

uint64
sys_getpid(void)
{
  return myproc()->pid;
}

uint64
sys_fork(void)
{
  return fork();
}

uint64
sys_wait(void)
{
  uint64 p;
  if(arg_ptr(0, &p) < 0)
    return -1;
  return wait(p);
}

uint64
sys_kill(void)
{
  int pid;

  if(arg_int(0, &pid) < 0)
    return -1;
  return kill(pid);
}

uint64
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(arg_int(0, &n) < 0)
    return -1;
  lock_spin(&ticklk);
  ticks0 = tick;
  while(tick - ticks0 < n){
    if(myproc()->killed){
      unlock_spin(&ticklk);
      return -1;
    }
    sleep(&tick, &ticklk);
  }
  unlock_spin(&ticklk);
  return 0;
}

uint64
sys_gettick(void)
{
  uint64 tick0;
  lock_spin(&ticklk);
  tick0 = tick; 
  unlock_spin(&ticklk);
  return tick0;
}

uint64
sys_uptime(void)
{
  return time();
}

uint64 sys_kprint()
{
  char buf[128];
  arg_str(0, buf, 128);
  printf("[user]: %s", buf);
  return 0;
}

uint64 sys_procdump()
{
  procdump();
  return 0;
}


extern uint64 sys_chdir(void);
extern uint64 sys_close(void);
extern uint64 sys_dup(void);
extern uint64 sys_exec(void);
// extern uint64 sys_exit(void);
// extern uint64 sys_fork(void);
extern uint64 sys_fstat(void);
// extern uint64 sys_getpid(void);
extern uint64 sys_kill(void);
extern uint64 sys_link(void);
extern uint64 sys_mkdir(void);
extern uint64 sys_mknod(void);
extern uint64 sys_open(void);
extern uint64 sys_pipe(void);
extern uint64 sys_read(void);
// extern uint64 sys_sbrk(void);
extern uint64 sys_sleep(void);
extern uint64 sys_unlink(void);
// extern uint64 sys_wait(void);
extern uint64 sys_write(void);
// extern uint64 sys_uptime(void);
// extern uint64 sys_kprint(void);
// extern uint64 sys_procdump(void);
extern uint64 sys_connect(void);

static uint64 (*syscalls[])(void) = {
[SYS_fork]    sys_fork,
[SYS_exit]    sys_exit,
[SYS_wait]    sys_wait,
[SYS_pipe]    sys_pipe,
[SYS_read]    sys_read,
[SYS_kill]    sys_kill,
[SYS_exec]    sys_exec,
[SYS_fstat]   sys_fstat,
[SYS_chdir]   sys_chdir,
[SYS_dup]     sys_dup,
[SYS_getpid]  sys_getpid,
[SYS_uptime]  sys_uptime,
[SYS_open]    sys_open,
[SYS_write]   sys_write,
[SYS_mknod]   sys_mknod,
[SYS_unlink]  sys_unlink,
[SYS_link]    sys_link,
[SYS_mkdir]   sys_mkdir,
[SYS_close]   sys_close,
[SYS_test]    sys_test,
[SYS_sbrk]    sys_sbrk,
[SYS_sleep]   sys_sleep,
[SYS_kprint]  sys_kprint,
[SYS_procdump]  sys_procdump,
[SYS_connect]  sys_connect,
[SYS_gettick] sys_gettick,
};


void syscall()
{
    struct proc *p = myproc();
    int num = p->trapframe->a7;
    // printf("syscall: %d\n", num);

    if(num > 0 && num <= SYSCALL_NUM && syscalls[num]) {
      int ret = syscalls[num]();
      p->trapframe->a0 = ret;
    } else {
      error("unknown sys call %d\n", num);
    }
}