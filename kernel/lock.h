#include "types.h"


#ifndef _LOCK_H
#define _LOCK_H

struct spinlock
{
    int locked;

    struct cpu *cpu;   // The cpu holding the lock.
    char *name;
};

struct sleeplock
{
    int locked;
    struct spinlock lock;
    
    int pid;
    char *name;
};

#endif