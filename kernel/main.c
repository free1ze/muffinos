#include "defs.h"
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "lock.h"
#include "defs.h"

static int started = 0;

// void page_dump(uint64 addr)
// {
// 	addr = PGROUNDDOWN(addr);
// 	char *p = (char *)addr;
// 	printf("addr= %p  satp=%x\n", addr, r_satp());
// 	for(int i=0; i<PGSIZE/16; i++) {
// 		printf("%x ", p[i]);
// 		// if(i % 16) {
// 		// 	printf("\n");
// 		// }
// 	}
// 	printf("\n");

// }

// void io_test()
// {
// 	uartputc_sync('h');
// 	uartputc_sync('i');
// 	uartputc_sync('\n');
// 	while (1)
//     {
//         int c = uartgetc();
//         if(c != -1) {
//             uartputc_sync(c);
// 			uartputc_sync('\n');
// 			uartputc_sync('$');
//         }
//     }
// }


void inits()
{
	if(cpuid() == 0){
		printf("***************************\n");
		printf("    Muffin OS Booting...   \n");
		printf("***************************\n");

		init_console();
		init_kmem();
		init_kvm();
		init_kvmhart();
		init_proc();
		init_trap();      // trap vectors
		init_traphart();  // install kernel trap vector
		init_plic();      // set up interrupt controller
		init_plichart();  // ask PLIC for device interrupts
		init_bio();         // buffer cache
		init_inode();         // inode table
		init_file();      // file table
		init_virtio_disk(); // emulated hard disk
		init_socket();
		init_pci();
		init_uproc();      // first user process
		__sync_synchronize();
		started = 1;
  	} else {
		while(started == 0)
			;
		__sync_synchronize();
		init_kvmhart();
		init_traphart();   // install kernel trap vector
		init_plichart();   // ask PLIC for device interrupts
	}


}

 
void main(void) {
	inits();
	// io_test();
	        
	// physical_memory_test();
  	scheduler();
	return;
}