#include "defs.h"
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "lock.h"

// physical memory allocator

extern char end[]; // first address after kernel.
                   // defined by kernel.ld.

struct ppage
{
    struct ppage *next;
};

struct ppagelist
{
    struct ppage *head;
    struct spinlock lk;
};

struct ppagelist kmem[NCPU];

static
inline uint64 hash()
{
    return cpuid();
}

void kfree(uint64 pg)
{
    int idx = hash(pg);
    lock_spin(&kmem[idx].lk);
    pg = PGROUNDDOWN(pg);
    memset((char *)pg, 0, PGSIZE);
    ((struct ppage *)pg)->next = kmem[idx].head;
    kmem[idx].head = (struct ppage *)pg;
    unlock_spin(&kmem[idx].lk);
}

static
void free_range(uint64 start, uint64 end)
{
    uint64 p = PGROUNDUP(start);
    for(; p+PGSIZE <=end; p+=PGSIZE) {
        kfree(p);
    }
}

void init_kmem()
{
    for(int i=0; i<NCPU; i++) {
        init_spinlock(&kmem[i].lk, "keme lock");
        kmem[i].head = NULL;
    }
    free_range((uint64)end, (uint64)PHYSTOP);
    log("kernel memory allocator init complete\n");
}

uint64 kalloc()
{
    uint64 idx = hash();
    uint64 pg = 0;
    for(int i = idx+1; i != idx; i++) {
        i %= NCPU;

        lock_spin(&kmem[i].lk);
        if(kmem[i].head == NULL) {
            unlock_spin(&kmem[i].lk);
            continue;
        }
        pg = (uint64)kmem[i].head;
        kmem[i].head = kmem[i].head->next;
        unlock_spin(&kmem[i].lk);
        memset((char *)pg, 0, PGSIZE);
        break;
    }

    assert(pg != 0);
    return pg;
}