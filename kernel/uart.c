//
// low-level driver routines for 16550a UART.
//

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "lock.h"
#include "proc.h"
#include "defs.h"

// the UART control registers.
// some have different meanings for
// read vs write.
// see http://byterunner.com/16550.html
#define RHR 0                 // receive holding register (for input bytes)
#define THR 0                 // transmit holding register (for output bytes)
#define IER 1                 // interrupt enable register
#define IER_RX_ENABLE (1<<0)
#define IER_TX_ENABLE (1<<1)
#define FCR 2                 // FIFO control register
#define FCR_FIFO_ENABLE (1<<0)
#define FCR_FIFO_CLEAR (3<<1) // clear the content of the two FIFOs
#define ISR 2                 // interrupt status register
#define LCR 3                 // line control register
#define LCR_EIGHT_BITS (3<<0)
#define LCR_BAUD_LATCH (1<<7) // special mode to set baud rate
#define LSR 5                 // line status register
#define LSR_RX_READY (1<<0)   // input is waiting to be read from RHR
#define LSR_TX_IDLE (1<<5)    // THR can accept another character to send

#define Reg(reg) ((volatile unsigned char *)(UART0 + reg))
#define ReadReg(reg) (*(Reg(reg)))
#define WriteReg(reg, v) (*(Reg(reg)) = (v))

struct spinlock uart_lock;
#define UART_BUF_SIZE 32
char uart_tx_buf[UART_BUF_SIZE];
uint64 uart_w;
uint64 uart_r;

volatile extern int on_error;

void init_uart(void)
{
  // disable interrupts.
  WriteReg(IER, 0x00);

  // special mode to set baud rate.
  WriteReg(LCR, LCR_BAUD_LATCH);

  // LSB for baud rate of 38.4K.
  WriteReg(0, 0x03);

  // MSB for baud rate of 38.4K.
  WriteReg(1, 0x00);

  // leave set-baud mode,
  // and set word length to 8 bits, no parity.
  WriteReg(LCR, LCR_EIGHT_BITS);

  // reset and enable FIFOs.
  WriteReg(FCR, FCR_FIFO_ENABLE | FCR_FIFO_CLEAR);

  // enable transmit and receive interrupts.
  WriteReg(IER, IER_TX_ENABLE | IER_RX_ENABLE);

  init_spinlock(&uart_lock, "uart lock");
}

// need lock held
void uart_transmit()
{
  while(uart_r < uart_w) {
    if(ReadReg(LSR) & LSR_TX_IDLE) {
      int c = uart_tx_buf[uart_r % UART_BUF_SIZE];
      uart_r++;
      WriteReg(THR, c);
    }
  }
}

// read one input character from the UART.
// return -1 if none is waiting.
int uartgetc(void)
{
  if(ReadReg(LSR) & LSR_RX_READY){
    // input data is ready.
    return ReadReg(RHR);
  } else {
    return -1;
  }
}

void uartputc_sync(char c) {

  // push_off();

  if(on_error) {
    for(;;) {
      ;
    }
  }

  while((ReadReg(LSR) & LSR_TX_IDLE) == 0)
        ;
  WriteReg(THR, c);

  // pop_off();
}

void uartputc(char c) 
{
    lock_spin(&uart_lock);

    if(on_error) {
      for(;;) {
        ;
      }
    }
    while(1) {
      // buf full
      if((uart_w - uart_r) == UART_BUF_SIZE ) {
        sleep(&uart_r, &uart_lock);
      } else {
        uart_tx_buf[uart_w % UART_BUF_SIZE] = c;
        uart_w++;
        wakeup(&uart_w);
        uart_transmit();
        break;
      }
  }
  
  unlock_spin(&uart_lock);
}



// intr when receiving a byte or having finished transmitted a byte
void uart_intr()
{
  int c;
  while((c = uartgetc()) != -1) {
    console_intr(c);
  }

  lock_spin(&uart_lock);
  uart_transmit();
  unlock_spin(&uart_lock);
}


