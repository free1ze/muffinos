#include "types.h"

#ifndef _DEFS_H
#define _DEFS_H

struct buf;
struct context;
struct file;
struct inode;
struct pipe;
struct proc;
struct spinlock;
struct sleeplock;
struct stat;
struct superblock;
struct mbuf;
struct mbufq;
struct socket;


// basic
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define abs(a)   ((a) >= 0 ? (a) : (-1 * (a)))

#ifndef assert
#define assert(x)     do {if(!(x)) { \
    printf("\n"); \
    error("assertion failed: (%s)\n", #x); \
    while(1); \
}}while(0)
#define staitc_assert(x)    do {switch(x) { case 0: case (x): ;} } while(0)
#endif

// uart.c
// void            uartinit(void);
// void            uartintr(void);
// void            uartputc_sync(int);
// int             uartgetc(void);
// void            uartputc(char);

// print.c
void printf(char *fmt, ...);
void mark();
void log(char *fmt, ...);
void error(char *fmt, ...);

// string.c
void memcpy(void *dst, const void *src, int len);
void memset(void *dst, char val, int len);
int strcmp(const char *p, const char *q);
char *strcpy(char *dst, const char *src);
char *strcat(char *dst, const char *src);
int strlen(const char *str);

// kalloc.c
void kfree(uint64 pg);
void init_kmem();
uint64 kalloc();

// proc.c
int cpuid();
struct proc* myproc();
struct cpu* mycpu();
void init_proc();
void init_uproc();
void scheduler();
void wakeup(void *chan);
void yield();
void backtrace();

void sleep(void *chan, struct spinlock *lk);
int wait(uint64 xstatus);
void exit(int xstate);
int kill(int pid);
int fork();

// exec.c
int exec(char *path, char **argv);

// plic.c
void init_plic(void);
void init_plichart(void);
int plic_claim(void);
void plic_complete(int irq);

// syscall.c
int arg_int(int idx, int *dst);
int arg_ptr(int idx, uint64 *dst);
int arg_str(int idx, char *buf, int len);
void syscall();

// vm.c
void init_kvm();
void init_kvmhart();
void dump_pgtbl(pagetable_t pgtbl);
void dump_page(pagetable_t pgtbl, uint64 va);
pagetable_t make_uvm(struct proc *p);
int vmmap(uint64 va, uint64 pa, pagetable_t pgtbl, int size, uint64 permission);
int vmunmap(uint64 va, pagetable_t pgtbl, int size, int do_free);
int uvmalloc(pagetable_t pgtbl, int oldsz, int n);
int uvmdealloc(pagetable_t pgtbl, int oldsz, int n);
int growproc(int n);
int copyin(pagetable_t pgtbl, uint64 src, uint64 dst, int len);
int copyout(pagetable_t pgtbl, uint64 src, uint64 dst, int len);
int either_copyout(int from_user, uint64 dst, uint64 src, uint64 len);
int either_copyin(int from_user, uint64 dst, uint64 src, uint64 len);
int copyinstr(pagetable_t pgtbl, uint64 src, uint64 dst, uint64 len);
void uvmclear(pagetable_t pagetable, uint64 va);
void free_pgtbl(pagetable_t pgtbl);
int uvmcopy(pagetable_t old, pagetable_t new, int sz);
uint64 walkaddr(uint64 va, pagetable_t pgtbl);

// lock.c
void init_spinlock(struct spinlock *lk, char *name);
void lock_spin(struct spinlock *lk);
void unlock_spin(struct spinlock *lk);
void init_sleeplock(struct sleeplock *lk, char *name);
void lock_sleep(struct sleeplock *lk);
void unlock_sleep(struct sleeplock *lk);
int holding_spin(struct spinlock *lk);
int holding_sleep(struct sleeplock *lk);
void procdump();
void push_off();
void pop_off();



// uart.c
void init_uart();
int uartgetc(void); 
void uartputc_sync(char c);
void uartputc(char c);
void uart_intr();

// console.c
void init_console();
void consputc(int c);
void console_intr(int c);
int consoleread(uint64 dst, int n);
int consolewrite(uint64 src, int n);

// trap.c
void init_trap();
void init_traphart();
void usertrapret();
extern struct spinlock ticklk;
extern uint64 tick;
uint64 time();

// pipe.c
int pipealloc(struct file **f0, struct file **f1);
void pipeclose(struct pipe *pp, int iswrite);
int pipewrite(struct pipe *pp, uint64 addr, int n);
int piperead(struct pipe *pp, uint64 addr, int n);


/* net */
// e1000.c
void e1000_init(uint32 *xregs);
void e1000_intr(void);
int e1000_transmit(struct mbuf *m);

// net.c
void init_mbufq(struct mbufq *q);
void mbufq_pushback(struct mbufq *q, struct mbuf *m);
struct mbuf *mbufq_popfront(struct mbufq *q);
int mbufq_empty(struct mbufq *q);
struct mbuf *mbufalloc();
void mbuffree(struct mbuf *m);
char *mbuf_wdata(struct mbuf *m, int len);
void send_udp(struct mbuf *m, uint32 dst_ip, uint16 dst_p, uint16 src_p);
void send_ip(struct mbuf *m, uint8 type, uint32 dst_ip);
void recv_pkt(struct mbuf *m);

// socket.c
void init_socket();
int socketread(struct socket *s, uint64 addr, int n);
int socketwrite(struct socket *s, uint64 addr, int n);
int socketclose(struct socket *s);
void push_packet(struct mbuf *m, uint32 dst_ip, uint16 dst_p, uint16 src_p, uint16 type);

// pci.c
void init_pci();

/* file system */
// virtio_disk.c
void init_virtio_disk();
void virtio_disk_rw(struct buf *b, int write);
void virtio_disk_intr();

// bio.c
void init_bio();
struct buf *bread(int bno);
void bwrite(struct buf *b);
void brelse(struct buf *b);
void bpin(struct buf *b);
void bunpin(struct buf *b);

// log.c 
void init_log();
struct inode *iget(uint inum);
int readi(uint64 dst, int from_user, struct inode *ip, int off, int n);
int writei(uint64 src, int from_user, struct inode *ip, int off, int n);
void logwrite(struct buf *b);
void begin_op();
void end_op();

// inode.c
void init_inode();
struct inode *iget(uint inum);
struct inode *ialloc(short type);
void iupdate(struct inode *ip);
struct inode *idup(struct inode *ip);
void ilock(struct inode *ip);
void iunlock(struct inode *ip);
void iput(struct inode *ip);
void itrunc(struct inode *ip);
void stati(struct inode *ip, struct stat *stat);

// dir.c
struct inode *dirlookup(struct inode *dip, char *name, uint *poff);
int dirlink(struct inode *dip, char *name, uint inum);

// name.c
struct inode* namei(char *path);
struct inode* nameiparent(char *path, char *name);

// file.c
void init_file();
int fileread(struct file *f, uint64 addr, int n);
int filewrite(struct file *f, uint64 addr, int n);
struct file *filealloc();
struct file* filedup(struct file *f);
struct file* fileclose(struct file *f);
int filestat(struct file *f, uint64 addr);
int fdalloc(struct file *f);

#endif