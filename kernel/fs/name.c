#include "../defs.h"
#include "../proc.h"
#include "inode.h"
#include "dir.h"

// Copy the next path element from path into name.
// Return a pointer to the element following the copied one.
// The returned path has no leading slashes,
// so the caller can check *path=='\0' to see if the name is the last one.
// If no name to remove, return 0.
//
// Examples:
//   skipelem("a/bb/c", name) = "bb/c", setting name = "a"

static char *skipelem(char *path, char *name)
{
    while(*path == '/') {
        path++;
    }
    if(*path == 0) {
        return 0;
    }
    int count = 0;
    while(*path != '/' && *path != 0) {
        assert(count++ < DIRSIZ);
        *name++ = *path++;
    }
    *name++ = 0;
    return path;
}

// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
struct inode* namex(char *path, char *name, int parent)
{
    struct inode *ip, *next;

    if(*path == '/')
        ip = iget(ROOTINO);
    else
        ip = idup(myproc()->cwd);

    while((path = skipelem(path, name)) != 0){
        ilock(ip);
        if(ip->type != T_DIR){
            iunlock(ip);
            iput(ip);
            return 0;
        }
        if(parent && *path == '\0'){
            // Stop one level early.
            iunlock(ip);
            return ip;
        }
        if((next = dirlookup(ip, name, 0)) == 0){
            iunlock(ip);
            iput(ip);
            return 0;
        }
        iunlock(ip);
        iput(ip);
        ip = next;
    }
    if(parent){
        iput(ip);
        return 0;
    }
    return ip;
}

struct inode* namei(char *path)
{
  char name[DIRSIZ];
  return namex(path, name, false);
}

struct inode* nameiparent(char *path, char *name)
{
  return namex(path, name, true);
}