#ifndef _LOG_H
#define _LOG_H


#define BOOTBLOCK       0
#define SUPERBLOCK      1

// variables below should be defined in mkfs/mkfs.c
// #define LOGBLOCKNUM     30
// #define INODEBLOCKNUM   13
// #define BITMAPBLOCK     (SUPERBLOCK + LOGBLOCKNUM + INODEBLOCKNUM + 1)

#define MAXOPBLOCKS  10
#define LOGSIZE  (MAXOPBLOCKS * 3)

#define FSMAGIC 0x10203040

// const data read from fs.img
struct superblock
{
    uint magic;
    uint size;      // total blocks
    uint nlog;
    uint ninode;
    uint ndata;
    uint logstart;
    uint inodestart;
    uint bmapstart;
    uint datastart;
};

struct logheader
{
    int n;
    int bno[LOGSIZE];
};


struct log
{
    struct logheader lh;

    struct spinlock lk;
    uint start;
    uint size;
    int using;

};



#endif