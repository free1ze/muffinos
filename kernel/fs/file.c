#include "../defs.h"
#include "../types.h"
#include "../lock.h"
#include "../param.h"
#include "../proc.h"
#include "dir.h"
#include "file.h"
#include "log.h"
#include "inode.h"

static
struct {
  struct spinlock lock;
  struct file file[NFILE];
} ftable;

void init_file()
{
    init_spinlock(&ftable.lock, "ftable lock");

    log("fs file init complete\n");
}

struct file *filealloc()
{
    lock_spin(&ftable.lock);
    struct file *f;
    for(f = ftable.file; f<&ftable.file[NFILE]; f++) {
        if(f->ref == 0) {
            f->ref++;
            unlock_spin(&ftable.lock);
            return f;
        }
    }
    unlock_spin(&ftable.lock);
    return 0;
}

int fdalloc(struct file *f)
{
    struct proc *p = myproc();

    for(int fd = 0; fd < NOFILE; fd++){
        if(p->ofile[fd] == 0){
            p->ofile[fd] = f;
            return fd;
        }
    }
    return -1;
}

struct file* filedup(struct file *f)
{
    lock_spin(&ftable.lock);
    assert(f->ref > 0);
    f->ref++;
    unlock_spin(&ftable.lock);
    return f;
}

struct file* fileclose(struct file *f)
{
    lock_spin(&ftable.lock);
    assert(f->ref > 0);
    f->ref--;
    if(f->ref == 0) {
        switch (f->type)
        {
            case FD_PIPE:
                pipeclose(f->pipe, f->writable);
                break;
            case FD_INODE:
            case FD_DEVICE:
                begin_op();
                iput(f->ip);   
                end_op();  
                break;
            case FD_SOCK:
                socketclose(f->sock);
                break;
            default:
                break;
        }
        f->type = FD_NONE;
    }
    unlock_spin(&ftable.lock);
    return f;
}

// addr point to a struct stat in user space 
int filestat(struct file *f, uint64 addr)
{
    struct proc *p = myproc();

    struct stat stat;
    ilock(f->ip);
    stati(f->ip, &stat);
    iunlock(f->ip);

    if(copyout(p->pagetable, (uint64)&stat, addr, sizeof(stat)) == -1) {
        return -1;
    }

    return 0;
}

int fileread(struct file *f, uint64 addr, int n)
{
    if(!f->readable) {
        return -1;
    }
    int r = 0;
    switch (f->type)
    {
        case FD_PIPE:
            r = piperead(f->pipe, addr, n);
            break;
        case FD_DEVICE:
            r = consoleread(addr, n);
            break;
        case FD_SOCK:
            r = socketread(f->sock, addr, n);
            break;
        case FD_INODE:
            ilock(f->ip);
            if( (r = readi(addr, true, f->ip, f->off, n) ) > 0){
                f->off += r;
            }
            iunlock(f->ip);
            break;
        default:
            error("unknown file type");
    }
    return r;
}

int filewrite(struct file *f, uint64 addr, int n)
{
    if(!f->writable) {
        return -1;
    }
    int r = 0;
    switch (f->type)
    {
        case FD_PIPE:
            r = pipewrite(f->pipe, addr, n);
            break;
        case FD_DEVICE:
            r = consolewrite(addr, n);
            break;
        case FD_SOCK:
            r = socketwrite(f->sock, addr, n);
            break;
        case FD_INODE:
            assert(n < (((MAXOPBLOCKS-1-1-2) / 2) * BSIZE));
            begin_op();
            ilock(f->ip);
            if( ((r = writei(addr, true, f->ip, f->off, n)) > 0)){
                f->off += r;
            }
            iunlock(f->ip);
            end_op();
            break;
        default:
            error("unknown file type");
    }
    return r;
}

