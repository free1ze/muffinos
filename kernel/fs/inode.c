#include "../defs.h"
#include "../types.h"
#include "../param.h"
#include "../proc.h"
#include "bio.h"
#include "log.h"
#include "inode.h"

extern struct superblock sb;

static 
void bzero(int bno)
{
    struct buf *b = bread(bno);
    memset(b->data, 0, BSIZE);
    logwrite(b);
    brelse(b);
}

int balloc()
{
    struct buf *bitmap = bread(sb.bmapstart);
    char *data = bitmap->data;
    for(int i = 0; i < sb.size; i++) {
        int mask = 1 << (i % 8);
        if((data[i / 8] & mask) == 0) {
            data[i / 8] |= mask;
            logwrite(bitmap);
            brelse(bitmap);
            bzero(i);
            return i;
        }
    }
    error("bitmap: no free block");
    return -1;
}

void bfree(int bno)
{
    struct buf *bitmap = bread(sb.bmapstart);
    char *data = bitmap->data;
    int mask = 1 << (bno % 8);
    assert((data[bno / 8] & mask) != 0);
    data[bno / 8] &= ~mask;
    logwrite(bitmap);
    brelse(bitmap);
}

struct {
  struct spinlock lock;
  struct inode inode[NINODE];
} itable;

void init_inode()
{
    init_spinlock(&itable.lock, "itable lock");
    for(int i=0; i<NINODE; i++) {
        init_sleeplock(&itable.inode[i].lock, "inode lock");
    }
    log("fs inode init complete\n");
}

inline static 
int iblock(int inum)
{
    return sb.inodestart + inum / NPB;
}

struct inode *iget(uint inum)
{
    lock_spin(&itable.lock);
    struct inode *nd;
    for(nd = itable.inode; nd < &itable.inode[NINODE]; nd++){
        if(nd->inum == inum) {
            nd->ref++;
            unlock_spin(&itable.lock);
            return nd;
        }
    }

    for(nd = itable.inode; nd < &itable.inode[NINODE]; nd++){
        if(nd->ref == 0) {
            nd->ref = 1;
            nd->valid = false;
            nd->inum = inum;
            unlock_spin(&itable.lock);
            return nd;
        }
    }

    error("iget: no free inode");
    return 0;
}

// alloc on disk inode by writing its type to 'type'
struct inode *ialloc(short type)
{
    struct buf *b;
    struct dinode *dnd; // disk node 
    for(int i=1; i<sb.ninode; i++) {
        b = bread(iblock(i));
        dnd = (struct dinode *)b->data + i % NPB;
        if(dnd->type == 0) {
            memset((char *)dnd, 0, sizeof(struct dinode));
            dnd->type = type;
            logwrite(b);
            brelse(b);
            struct inode *ip = iget(i);
            return ip;
            // return iget(i);
        }
        brelse(b);
    }
    error("no inode");
    return 0;
}

// write to disk inode 
// called with ip lock held
void iupdate(struct inode *ip)
{
    struct buf *b = bread(iblock(ip->inum));
    struct dinode *dnd = (struct dinode *)b->data + ip->inum % NPB;
    dnd->type = ip->type;
    dnd->major = ip->major;
    dnd->minor = ip->minor;
    dnd->nlink = ip->nlink;
    dnd->size = ip->size;
    memcpy((char *)dnd->addrs, (char *)ip->addrs, sizeof(ip->addrs));
    logwrite(b);
    brelse(b);
}

struct inode *idup(struct inode *ip)
{
    lock_spin(&itable.lock);
    ip->ref++;
    unlock_spin(&itable.lock);
    return ip;
}

// acquire sleep lock of ip
// fetch data from disk if (!ip->valid)
void ilock(struct inode *ip)
{
    assert(!holding_sleep(&ip->lock));

    lock_sleep(&ip->lock);

    if(ip == 0 || ip->ref < 1) {
        error("ilock");
    }

    if(!ip->valid) {
        struct buf *b = bread(iblock(ip->inum));
        struct dinode *dnd = (struct dinode *)b->data + ip->inum % NPB;
        ip->type = dnd->type;
        ip->major = dnd->major;
        ip->minor = dnd->minor;
        ip->nlink = dnd->nlink;
        ip->size = dnd->size;
        memcpy((char *)ip->addrs, (char *)dnd->addrs, sizeof(ip->addrs));
        assert(ip->type != 0);
        brelse(b);
        ip->valid = true;
    }
}

void iunlock(struct inode *ip)
{
    assert(holding_sleep(&ip->lock));
    unlock_sleep(&ip->lock);
}

// dec ip->ref, if ip->ref==0 then call iupdate 
void iput(struct inode *ip)
{
    lock_spin(&itable.lock);
    ip->ref--;
    if(ip->ref == 0 && ip->nlink == 0 && ip->valid) {
        lock_sleep(&ip->lock);
        itrunc(ip);
        ip->type = 0;
        ip->valid = 0;
        iupdate(ip);
        unlock_sleep(&ip->lock);
    }
    unlock_spin(&itable.lock);
}

// called when sleep lock held
void itrunc(struct inode *ip)
{
    struct buf *b;
    for(int i=0; i<NDIRECT; i++){
        if(ip->addrs[i] != 0) {
            bfree(ip->addrs[i]);
            ip->addrs[i] = 0;
        }
    }
    if(ip->addrs[NDIRECT] != 0) {
        b = bread(ip->addrs[NDIRECT]);
        uint *data = (uint *)b->data;
        for(int i=0; i<NINDIRECT; i++) {
            if(data[i] != 0) {
                bfree(data[i]);
            }
        }
        brelse(b);
        bfree(ip->addrs[NDIRECT]);
        ip->addrs[NDIRECT] = 0;
    } 
    ip->size = 0;
    iupdate(ip);
}

uint imap(struct inode *ip, int ibno)
{
    uint bno = -1;
    if(ibno < NDIRECT) {
        bno = ip->addrs[ibno];
        if(bno == 0) {
            bno = balloc();
            ip->addrs[ibno] = bno;
        }
    } else if(ibno < MAXFILE){
        bno = ip->addrs[NDIRECT];
        if(bno == 0) {
            bno = balloc();
            ip->addrs[NDIRECT] = bno;
        }
        struct buf *b = bread(bno);
        uint *data = (uint *)b->data;
        bno = data[ibno - NDIRECT];
        if(bno == 0) {
            bno = balloc();
            b->data[ibno - NDIRECT] = bno;
            logwrite(b);
        }
        brelse(b);
    } else {
        error("imap: ibno out of range");
    }
    return bno;
}

int readi(uint64 dst, int from_user, struct inode *ip, int off, int n)
{
    // assert(off < ip->size);
    // assert(off + n < ip->size);
    if(off > ip->size || off + n < off) {
        return 0;
    }
    if(off + n > ip->size) {
        n = ip->size - off;
    }
    uint tot,m;
    struct buf *b;
    for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
        b = bread(imap(ip, off / BSIZE));
        m = min(n - tot, BSIZE - off%BSIZE);
        if(either_copyout(from_user, dst, (uint64)(b->data + (off % BSIZE)), m) == -1) {
            brelse(b);
            return -1;
        }
        brelse(b);
    }
    return tot;
}

int writei(uint64 src, int from_user, struct inode *ip, int off, int n)
{
    assert(off <= ip->size);
    assert(off + n < MAXFILE * BSIZE);

    uint tot,m;
    struct buf *b;
    for(tot=0; tot<n; tot+=m, off+=m, src+=m){
        b = bread(imap(ip, off / BSIZE));
        m = min(n - tot, BSIZE - off%BSIZE);
        if(either_copyin(from_user, (uint64)(b->data + (off % BSIZE)), src, m) == -1) {
            brelse(b);
            break;
        }
        logwrite(b);
        brelse(b);
    }

    if(off > ip->size) {
        ip->size = off;
    }
    iupdate(ip);
    return tot;
}

// syscall stat
void stati(struct inode *ip, struct stat *stat)
{
    stat->inum = ip->inum;
    stat->nlink = ip->nlink;
    stat->size = ip->size;
    stat->type = ip->type;
}


