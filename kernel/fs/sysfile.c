#include "../defs.h"
#include "../types.h"
#include "../lock.h"
#include "../param.h"
#include "../proc.h"
#include "../memlayout.h"
#include "dir.h"
#include "file.h"
#include "log.h"
#include "inode.h"
#include "fctrl.h"

static int argfd(int n, int *pfd, struct file **pf)
{
    int fd;
    struct file *f;

    if(arg_int(n, &fd) < 0)
        return -1;
    if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
        return -1;
    if(pfd)
        *pfd = fd;
    if(pf)
        *pf = f;
    return 0;
}

// return file descriptor
uint64 sys_dup()
{
    struct file *f;
    int fd;

    if(argfd(0, 0, &f) < 0) {
        return -1;
    }
    if( (fd = fdalloc(f))< 0) {
        return -1;
    }
    filedup(f);
    return fd;
}

uint64 sys_read()
{
    uint64 addr;
    int n;
    struct file *f;

    if(argfd(0, 0, &f) < 0 || arg_ptr(1, &addr) < 0 || arg_int(2, &n) < 0) {
        return -1;
    }
    return fileread(f, addr, n);
}

uint64 sys_write()
{
    uint64 addr;
    int n;
    struct file *f;

    if(argfd(0, 0, &f) < 0 || arg_ptr(1, &addr) < 0 || arg_int(2, &n) < 0) {
        return -1;
    }

    return filewrite(f, addr, n);
}

uint64 sys_close()
{
    struct file *f;
    int fd;

    if(argfd(0, &fd, &f) < 0) {
        return -1;
    }
    myproc()->ofile[fd] = 0;
    fileclose(f);
    return 0;
}

uint64 sys_fstat()
{
    struct file *f;
    uint64 addr;
    if(argfd(0, 0, &f) < 0 || arg_ptr(1, &addr) < 0 ) {
        return -1;
    }
    return filestat(f, addr);
}

uint64 sys_link()
{
    error("unimplemented");
    return 0;
}

int isdirempty(struct inode *dp)
{
    int isempty = true;
    struct dirent de;
    for(int i=0; i<dp->size; i+=sizeof(de)) {
        if( readi((uint64)&de, 0, dp, i, sizeof(de)) != sizeof(de) ) {
            error("isdirempty: readi error");
        }
        if(strcmp(de.name, ".") != 0 && strcmp(de.name, "..") != 0) {
            isempty = false;
        } 
    }
    return isempty;
}

uint64 sys_unlink()
{
    char path[MAXPATH], name[MAXPATH];
    if(arg_str(0, path,MAXPATH) < 0) {
        return -1;
    }
    begin_op();
    struct inode *ip, *dp;
    if((dp = nameiparent(path, name)) == 0) {
        end_op();
        return -1;
    }

    ilock(dp);

    if(strcmp(name, ".") == 0 || strcmp(name, "..") == 0) {
        goto bad;
    }
    uint off;
    if((ip = dirlookup(dp, name, &off)) == 0) {
       goto bad;
    }
    ilock(ip);
    assert(ip->nlink >= 1);
    if(ip->type == T_DIR && !isdirempty(ip)){
        iunlock(ip);
        iput(ip);
        goto bad;
    }

    struct dirent de;
    memset(&de, 0, sizeof(de));
    assert(writei((uint64)&de, false, dp, off, sizeof(de)) == sizeof(de));
    if(ip->type == T_DIR){
        dp->nlink--;
        iupdate(dp);
    }
    iunlock(dp);
    iput(dp);

    ip->nlink--;
    iupdate(ip);
    iunlock(ip);
    iput(ip);

    end_op();

    return 0;

    bad:
    iunlock(dp);
    iput(dp);
    end_op();
    return -1;
}


static struct inode*
create(char *path, short type, short major, short minor)
{
    struct inode *ip, *dp;
    char name[DIRSIZ];

    if((dp = nameiparent(path, name)) == 0)
        return 0;

    ilock(dp);

    if((ip = dirlookup(dp, name, 0)) != 0){
        iunlock(dp);
        iput(dp);
        ilock(ip);
        if(type == T_FILE && (ip->type == T_FILE || ip->type == T_DEVICE)) {
            return ip;
        }
        iunlock(ip);
        iput(ip);
        return 0;
    }

    if((ip = ialloc(type)) == 0) {
        error("create: ialloc");
    }

    ilock(ip);
    ip->major = major;
    ip->minor = minor;
    ip->nlink = 1;
    iupdate(ip);

    if(type == T_DIR){  // Create . and .. entries.
        dp->nlink++;  // for ".."
        iupdate(dp);
        // No ip->nlink++ for ".": avoid cyclic ref count.
        if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
        error("create dots");
    }

    if(dirlink(dp, name, ip->inum) < 0){
        error("create: dirlink");
    }

    iunlock(dp);
    iput(dp);

    return ip;
}

uint64 sys_open()
{
    char path[MAXPATH];
    int fd, omode;
    struct file *f;
    struct inode *ip;
    int n;

    if((n = arg_str(0, path, MAXPATH)) < 0 || arg_int(1, &omode) < 0) {
        return -1;
    }
    begin_op();

    if( omode & O_CREATE ){
        if ( (ip = create(path, T_FILE, 0, 0)) == 0) {
            end_op();
            return -1;
        }
    } else {
        if( (ip = namei(path)) == 0) {
            end_op();
            return -1;
        }
        ilock(ip);
        if(ip->type == T_DIR && omode != O_RDONLY){
            iunlock(ip);
            iput(ip);
            end_op();
            return -1;
        }
    }
    if(ip->type == T_DEVICE && (ip->major < 0 || ip->major >= NDEV)){
        iunlock(ip);
        iput(ip);
        end_op();
        return -1;
    }

    if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
        if(f) {
            fileclose(f);
        }
        iunlock(ip);
        iput(ip);
        end_op();
        return -1;
    }

    if(ip->type == T_DEVICE){
        f->type = FD_DEVICE;
        f->major = ip->major;
    } else {
        f->type = FD_INODE;
        f->off = 0;
    }
    f->ip = ip;
    f->readable = !(omode & O_WRONLY);
    f->writable = (omode & O_WRONLY) || (omode & O_RDWR);

    if((omode & O_APPEND) && ip->type == T_FILE) {
        f->off = ip->size;
    }

    if((omode & O_TRUNC) && ip->type == T_FILE){
        itrunc(ip);
    }
    iunlock(ip);
    end_op();

    return fd;
}

uint64 sys_mkdir()
{
  char path[MAXPATH];
  struct inode *ip;

  begin_op();
  if(arg_str(0, path, MAXPATH) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
    end_op();
    return -1;
  }
  iunlock(ip);
  iput(ip);
  end_op();
  return 0;
}

uint64
sys_mknod(void)
{
  struct inode *ip;
  char path[MAXPATH];
  int major, minor;

  begin_op();
  if((arg_str(0, path, MAXPATH)) < 0 ||
     arg_int(1, &major) < 0 ||
     arg_int(2, &minor) < 0 ||
     (ip = create(path, T_DEVICE, major, minor)) == 0){
    end_op();
    return -1;
  }
  iunlock(ip);
  iput(ip);
  end_op();

  return 0;
}

uint64 sys_chdir(void)
{
    char path[MAXPATH];
    struct inode *ip;
    struct proc *p = myproc();
    
    begin_op();
    if(arg_str(0, path, MAXPATH) < 0 || (ip = namei(path)) == 0){
        end_op();
        return -1;
    }
    ilock(ip);
    if(ip->type != T_DIR){
        iunlock(ip);
        iput(ip);
        end_op();
        return -1;
    }
    iunlock(ip);
    iput(p->cwd);
    end_op();
    p->cwd = ip;
    return 0;
}

uint64 sys_exec(void)
{
    char path[MAXPATH], *argv[MAXARG];
    int i;
    uint64 uargv, uarg;

    if(arg_str(0, path, MAXPATH) < 0 || arg_ptr(1, &uargv) < 0){
        return -1;
    }
    memset(argv, 0, sizeof(argv));
    uint64 addr = kalloc();
    for(i=0;; i++){
        if(i >= MAXARG){
            printf("exec: too many args\n");
            goto fail;
        }
        if(copyin(myproc()->pagetable, uargv+sizeof(uint64)*i, (uint64)&uarg, sizeof(uint64)) < 0){
            goto fail;
        }
        if(uarg == 0){
            argv[i] = 0;
            break;
        }
        argv[i] = (char *)(addr + MAXPATH*i);
        if(copyinstr(myproc()->pagetable, uarg, (uint64)argv[i], MAXPATH) < 0)
            goto fail;
    }

    int ret = exec(path, argv);

    kfree(addr);

    return ret;

    fail:
    kfree(addr);
    return -1;
}

uint64 sys_pipe()
{
    struct proc *p = myproc();
    uint64 fds;
    struct file *rf, *wf;
    int fd0 = -1, fd1 = -1;

    if(arg_ptr(0, &fds) < 0) {
        return -1;
    }
    if(pipealloc(&rf, &wf) < 0) {
        return -1;
    }
    if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0) {
        goto fail;
    }
    if((copyout(p->pagetable, (uint64)&fd0, fds, sizeof(fd0)) < 0) || 
        (copyout(p->pagetable, (uint64)&fd1, fds+sizeof(fd0), sizeof(fd1)) < 0)) {
        goto fail;
    }

    return 0;

    fail:
    if(fd0 >= 0){
        p->ofile[fd0] = 0;
    }
    if(fd1 >= 0){
        p->ofile[fd1] = 0;
    }
    fileclose(rf);
    fileclose(wf);
    return -1;
}