#include "../defs.h"
#include "../types.h"
#include "../param.h"
#include "../lock.h"
#include "inode.h"
#include "dir.h"

// if poff != 0, set *pe = dirent
// return inode pointer
// return NULL of not found
struct inode *dirlookup(struct inode *dip, char *name, uint *poff)
{
    assert(dip->type == T_DIR);
    struct dirent de;
    for(int i=0; i<dip->size; i+= sizeof(de)) {
        assert( readi((uint64)&de, false, dip, i, sizeof(de)) == sizeof(de));
        if( strcmp(name, de.name) == 0 ) {
            if(poff) {
                *poff = i;
            }
            return iget(de.inum);
        }
    }
    return NULL;
}

// Write a new directory entry (name, inum) into the directory dp.
int dirlink(struct inode *dip, char *name, uint inum)
{
    struct inode *ip;
    // name already exists
    if((ip = dirlookup(dip, name, 0)) != 0){
        iput(ip);
        return -1;
    }

    struct dirent e, de;
    e.inum = inum;
    memcpy(e.name, name, DIRSIZ);
    int i;
    for(i=0; i<dip->size; i+=sizeof(e)) {
        assert( readi((uint64)&de, false, dip, i, sizeof(e)) == sizeof(e));
        // found enmpty entry
        if(de.inum == 0) {
            break;
        }
    }
    assert( writei((uint64)&e, false, dip, i, sizeof(e)) == sizeof(e));
    return 0;
}


// Write a new directory entry (name, inum) into the directory dp.
// int
// dirlink(struct inode *dp, char *name, uint inum)
// {
//   int off;
//   struct dirent de;
//   struct inode *ip;

//   // Check that name is not present.
//   if((ip = dirlookup(dp, name, 0)) != 0){
//     iput(ip);
//     return -1;
//   }

//   // Look for an empty dirent.
//   for(off = 0; off < dp->size; off += sizeof(de)){
//     if(readi((uint64)&de, false, dp, off, sizeof(de)) != sizeof(de))
//       error("dirlink read");
//     if(de.inum == 0)
//       break;
//   }

//   memcpy(de.name, name, DIRSIZ);
//   de.inum = inum;
//   if(writei((uint64)&de, false, dp, off, sizeof(de)) != sizeof(de)) {
//     error("dirlink");
//   }

//   return 0;
// }