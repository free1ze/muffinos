#include "bio.h"

#ifndef _INODE_H
#define _INODE_H

#define NINODE  50

#define ROOTINO 1

#define NDIRECT 12
#define NINDIRECT (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT)

#define NPB     (BSIZE / sizeof(struct dinode))       // node per block (on disk)

// In-memory inode structure
struct inode {
  uint inum;
  int ref;
  struct sleeplock lock; 
  int valid;          // inode has been read from disk?

  short type;
  short major;
  short minor;
  short nlink;
  uint size;
  uint addrs[NDIRECT+1];
};

// On-disk inode structure
struct dinode {
    short type;           // File type
    short major;          // Major device number (T_DEVICE only)
    short minor;          // Minor device number (T_DEVICE only)
    short nlink;          // Number of links to inode in file system
    uint size;            // Size of file (bytes)
    uint addrs[NDIRECT+1];   // Data block addresses
};

// for stat syscall
struct stat {
    // int dev;     // File system's disk device
    uint inum;    // Inode number
    short type;  // Type of file
    short nlink; // Number of links to file
    uint64 size; // Size of file in bytes
};

#endif