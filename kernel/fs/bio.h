#include "../param.h"
#include "../lock.h"

#ifndef _BIO_H
#define _BIO_H

struct buf
{
    char data[BSIZE];
    int bno;
    int disk;       // does disk own the buf
    // int vaild;      // if the block alredy read from disk
    uint64 refcnt;
    struct sleeplock lk;

    // double linked list
    struct buf *prev;
    struct buf *next;

};

#endif