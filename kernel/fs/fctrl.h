#ifndef _FCTRL_H
#define _FCTRL_H

#define O_RDONLY  0x000     // Open for reading only
#define O_WRONLY  0x001     // Open for writing only
#define O_RDWR    0x002     // Open for reading and writing.
#define O_CREATE  0x200
#define O_TRUNC   0x400
#define O_APPEND  0x800


#endif