#include "../defs.h"
#include "../types.h"
#include "../param.h"
#include "../lock.h"
#include "bio.h"


// buf layer
struct {
    struct buf buf[NBUF];
    struct spinlock b_lk;

    // empty buf as header of double linked list
    struct buf head;
} bcache;

void init_bio()
{
    init_spinlock(&(bcache.b_lk), "buf lock");
    init_sleeplock(&bcache.head.lk, "buf block lock");
    bcache.head.next = &bcache.head;
    bcache.head.prev = &bcache.head;

    struct buf *b;
    for(b = bcache.buf; b<&bcache.buf[NBUF]; b++) {
        init_sleeplock(&b->lk, "buf block lock");
        b->next = bcache.head.next;
        b->prev = &bcache.head;
        bcache.head.next->prev = b;
        bcache.head.next = b;
    }

    log("fs buffer init complete\n");
}

struct buf *bread(int bno)
{
    assert(bno != 0);
    lock_spin(&bcache.b_lk);
    struct buf *b;
    for(b = bcache.head.next; b != &bcache.head; b = b->next) {
        if(b->bno == bno) {
            b->refcnt++;
            unlock_spin(&bcache.b_lk);
            lock_sleep(&b->lk);
            return b;
        }
    }

    for(b = bcache.head.prev; b != &bcache.head; b = b->prev) {
        if(b->refcnt == 0) {
            b->refcnt = 1;
            b->bno = bno;
            unlock_spin(&bcache.b_lk);
            lock_sleep(&b->lk);
            virtio_disk_rw(b, false);
            return b;
        }
    }

    error("bio: no free buf");
    unlock_spin(&bcache.b_lk);
    return 0;
}

void bwrite(struct buf *b)
{
    assert(holding_sleep(&b->lk));
    virtio_disk_rw(b, true);
}

void brelse(struct buf *b)
{
    assert(holding_sleep(&b->lk));
    unlock_sleep(&b->lk);

    lock_spin(&bcache.b_lk);
    b->refcnt--;
    if(b->refcnt == 0) {
        b->prev->next = b->next;
        b->next->prev = b->prev;
        b->next = bcache.head.next;
        b->prev = &bcache.head;
        bcache.head.next->prev = b;
        bcache.head.next = b;
    }
    unlock_spin(&bcache.b_lk);
}

void bpin(struct buf *b)
{
    lock_spin(&bcache.b_lk);
    b->refcnt++;
    unlock_spin(&bcache.b_lk);
}

void bunpin(struct buf *b)
{
    lock_spin(&bcache.b_lk);
    b->refcnt--;
    unlock_spin(&bcache.b_lk);
}

