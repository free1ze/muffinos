#include "../types.h"

#ifndef _DIR_H
#define _DIR_H

#define T_DIR     1   // Directory
#define T_FILE    2   // File
#define T_DEVICE  3   // Device

#define DIRSIZ  14


struct dirent
{
    ushort inum;
    char name[DIRSIZ];
};

#endif