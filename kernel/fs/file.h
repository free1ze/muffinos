#ifndef _FILE_H
#define _FILE_H

struct file {
    enum { FD_NONE, FD_PIPE, FD_INODE, FD_DEVICE , FD_SOCK} type;
    int ref; // reference count
    char readable;
    char writable;
    struct pipe *pipe;  // FD_PIPE
    struct inode *ip;   // FD_INODE and FD_DEVICE
    int off;           // FD_INODE
    short major;        // FD_DEVICE
    struct socket *sock; // FD_SOCK
};

#define major(dev)  ((dev) >> 16 & 0xFFFF)
#define minor(dev)  ((dev) & 0xFFFF)
#define	mkdev(m,n)  ((uint)((m)<<16| (n)))

#endif