#include "../defs.h"
#include "../types.h"
#include "../param.h"
#include "../lock.h"
#include "bio.h"
#include "log.h"

struct superblock sb;
struct log logger;

void readsb()
{
    struct buf *b = bread(SUPERBLOCK);
    memcpy((char *)&sb, b->data, sizeof(sb));
    assert(sb.magic == FSMAGIC);
    brelse(b);
}

void readlh()
{
    struct buf *b = bread(sb.logstart);
    memcpy((char *)&logger.lh, b->data, sizeof(struct logheader));
    brelse(b);
}


static void
install_trans(int recovering)
{
    for (int i = 0; i < logger.lh.n; i++) {
        struct buf *from = bread(logger.start+i+1);
        struct buf *to = bread(logger.lh.bno[i]);
        memcpy(from->data, to->data, BSIZE);
        bwrite(to);
        if(!recovering) {
            bunpin(to);
        }
        brelse(to);
        brelse(from);
    }
}

void write_head()
{
    struct buf *b = bread(sb.logstart);
    memcpy(b->data, (char *)&logger.lh, sizeof(struct logheader));
    bwrite(b);
    brelse(b);
}

void write_log()
{
    for (int i = 0; i < logger.lh.n; i++) {
        struct buf *to = bread(logger.start+i+1);
        struct buf *from = bread(logger.lh.bno[i]);
        memcpy(to->data, from->data, BSIZE);
        bwrite(to);
        brelse(to);
        brelse(from);
    }
}

// need logger.lk held
void commit()
{
    if (logger.lh.n > 0) {
        write_log();
        write_head();
        install_trans(0);
        logger.lh.n = 0;    // real commit point
        write_head();
    }
}

void recover_from_log()
{
    install_trans(true);
    logger.lh.n = 0;
    write_head();
}

void init_log()
{
    readsb();
    logger.start = sb.logstart;
    logger.size = sb.nlog;
    init_spinlock(&logger.lk, "log lock");
    readlh();
    recover_from_log();

    log("fs log init complete\n");
}

void begin_op()
{
    lock_spin(&logger.lk);
    while(1) {
        if(logger.using ) {
            sleep(&logger, &logger.lk);
        } else {
            logger.using = true;
            unlock_spin(&logger.lk);
            return;
        }
    }
}

void end_op()
{
    lock_spin(&logger.lk);
    assert(logger.using);
    commit();
    logger.using = false;
    wakeup(&logger);
    unlock_spin(&logger.lk);
}

void logwrite(struct buf *b)
{
    lock_spin(&logger.lk);
    assert(logger.lh.n < LOGSIZE);
    for(int i=0; i<logger.lh.n; i++) {
        if(logger.lh.bno[i] == b->bno) {
            // write absorbtion
            unlock_spin(&logger.lk);
            return;
        }
    }
    bpin(b);
    logger.lh.bno[logger.lh.n++] = b->bno;
    unlock_spin(&logger.lk);
}