#include "defs.h"
#include "types.h"
#include "proc.h"
#include "param.h"
#include "riscv.h"
#include "memlayout.h"
#include "lock.h"


// a user program that calls exec("/init")
// od -t xC initcode
static uchar initcode[] = {
  0x17, 0x05, 0x00, 0x00, 0x13, 0x05, 0x45, 0x02,
  0x97, 0x05, 0x00, 0x00, 0x93, 0x85, 0x35, 0x02,
  0x93, 0x08, 0x70, 0x00, 0x73, 0x00, 0x00, 0x00,
  0x93, 0x08, 0x20, 0x00, 0x73, 0x00, 0x00, 0x00,
  0xef, 0xf0, 0x9f, 0xff, 0x2f, 0x69, 0x6e, 0x69,
  0x74, 0x00, 0x00, 0x24, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
};


struct proc proc[NPROC];
struct cpu cpus[NCPU];

struct proc* initp;

static struct spinlock pid_lock;
static int nextpid = 1;

static struct spinlock wait_lock;

void forkret(void);

int cpuid()
{
  int id = r_tp();
  return id;
}

struct cpu* mycpu()
{
    // register tp holds the hartid
    uint64 hartid = r_tp();
    return &(cpus[hartid]);
}

struct proc* myproc()
{
    push_off();
    struct proc *p = mycpu()->proc;
    pop_off();
    // assert(p != NULL);
    return p;
}

int alloc_pid()
{
    lock_spin(&pid_lock);
    int pid = nextpid;
    nextpid++;
    unlock_spin(&pid_lock);
    return pid;
}

extern pagetable_t alloc_pagetable();

// return with p->lock hold
static
struct proc* make_proc(char *name)
{
    struct proc *nproc = NULL;
    struct proc *p;
    for(p = proc; p < &proc[NPROC]; p++) {
        lock_spin(&p->lk);
        if(p->state == UNUSED) {
            nproc = p;
            break;
        } else {
            unlock_spin(&p->lk);
        }
    }

    if( nproc == NULL ) {
        error("no proc available\n");
    }
    if( (nproc->pid = alloc_pid()) == NULL ) {
        error("pid alloc error\n");
    }
    nproc->state = RUNNABLE;
    if( (nproc->pagetable = make_uvm(nproc)) == NULL ) {
        error("pgtbl alloc error\n");
    }

    // Set up new context to start executing at forkret,
    // which returns to user space.
    memset((char *)&nproc->context, 0, sizeof(nproc->context));
    strcpy(nproc->name, name);
    nproc->context.ra = (uint64)forkret;
    nproc->context.sp = nproc->kstack + PGSIZE;

    // don't unlock !!
    return nproc;
}

// user syscall
void procdump(void)
{
  static char *states[] = {
  [UNUSED]    = "unused  ",
  [SLEEPING]  = "sleeping",
  [RUNNABLE]  = "runnable",
  [RUNNING]   = "running ",
  [ZOMBIE]    = "zombie  "
  };
  struct proc *p;

  printf("PID\t\tSTATUS\t\t\tNAME\n");

  for(p = proc; p < &proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    printf("%d\t\t%s\t\t%s\n",p->pid, states[p->state], p->name);
  }
}

void init_proc()
{
    struct proc *p;
    for(p = proc; p < &proc[NPROC]; p++){
        init_spinlock(&p->lk, "proc lock");
        p->kstack = KSTACK((int) (p - proc));
    }
    init_spinlock(&pid_lock, "pid lock");
    init_spinlock(&wait_lock, "wait lock");
    
    log("proc table init complete\n");
}

void init_uproc()
{
    initp = make_proc("init");
    
    assert(sizeof(initcode) < PGSIZE);
    uint64 mem = kalloc();
    memcpy((char *)mem, (char *)initcode, sizeof(initcode));
    vmmap(0, mem, initp->pagetable, PGSIZE, PTE_R | PTE_W | PTE_X | PTE_U);
    initp->sz = PGSIZE;

    initp->state = RUNNABLE;
    initp->cwd = namei("/");
    initp->trapframe->epc = 0;
    initp->trapframe->sp = PGSIZE + initp->kstack;
    unlock_spin(&initp->lk);
    log("first user proc init complete\n");
}

void free_proc(struct proc *p)
{
    if(p->trapframe) {
        kfree((uint64)p->trapframe);
    }
    p->trapframe = 0;
    if(p->pagetable) {
        free_pgtbl(p->pagetable);
    }
    p->pagetable = 0;
    p->sz = 0;
    p->pid = 0;
    p->parent = 0;
    p->chan = 0;
    p->killed = 0;
    p->xstate = 0;
    p->state = UNUSED;
    memset((char *)&p->context, 0, sizeof(p->context));
    memset(p->name, 0, sizeof(p->name));
}

// in swtch.S
extern void swtch(struct context *old, struct context *new);

// must be called with p->lock held
void sched()
{
    struct cpu *c = mycpu();
    struct proc *p = myproc();

    assert(holding_spin(&p->lk));
    assert(p->state != RUNNING);
    assert(intr_get() == false);

    int intena = c->intena;
    swtch(&p->context, &c->context);
    c->intena = intena;
}

void yield()
{
    struct proc *p = myproc();
    lock_spin(&p->lk);
    p->state = RUNNABLE;
    sched();
    unlock_spin(&p->lk);
}

void scheduler()
{
    struct cpu *c = mycpu();
    c->proc = 0;
    struct proc *p;
    while (1)
    {
        // Avoid deadlock by ensuring that devices can interrupt.
        intr_on();
        for(p = proc; p < &proc[NPROC]; p++){
            lock_spin(&p->lk);
            if(p->state == RUNNABLE) {
                c->proc = p;
                p->state = RUNNING;
                swtch(&c->context, &p->context);
                c->proc = 0;
            }
            unlock_spin(&p->lk);

        }
    }
    
}

void sleep(void *chan, struct spinlock *lk)
{
    assert(chan != 0);
    struct proc *p = myproc();
    // to avoid miss wakeups
    lock_spin(&p->lk);
    unlock_spin(lk);
    
    p->chan = chan;
    p->state = SLEEPING;

    sched();

    p->chan = 0;

    unlock_spin(&p->lk);
    lock_spin(lk);
}

// Wake up all processes sleeping on chan.
// Must be called without any p->lock.
void wakeup(void *chan)
{
    // struct proc *p = myproc();
    // printf("wake up porc on %p\n", (uint64)chan);

    struct proc *p;
    for(p = proc; p < &proc[NPROC]; p++){
        if(p == myproc()) {
            continue;
        }
        lock_spin(&p->lk);
        if(p->chan == chan && p->state == SLEEPING) {
            p->chan = 0;
            p->state = RUNNABLE;
        }
        unlock_spin(&p->lk);
    }
}

// return pid of exited child
// return -1 if no child
// return -2 on error
int wait(uint64 xstatus)
{
    struct proc *p = myproc();
    struct proc *np;
    int haschild = false;

    lock_spin(&wait_lock);
    while (1)
    {
        for(np = proc; np<&proc[NPROC]; np++) {
            if(np->parent == p) {
                lock_spin(&np->lk);
                haschild = true;
                if(np->state == ZOMBIE) {
                    // xstatus is from user space, use copyout
                    if( copyout(p->pagetable, (uint64)&p->xstate, xstatus, sizeof(p->xstate)) == -1 ) {
                        unlock_spin(&wait_lock);
                        unlock_spin(&np->lk);
                        return -2;
                    }
                    int pid = np->pid;
                    free_proc(np);
                    unlock_spin(&wait_lock);
                    unlock_spin(&np->lk);
                    return pid;
                }
                unlock_spin(&np->lk);
            }
        }
        if(!haschild || p->killed) {
            unlock_spin(&wait_lock);
            return -1;
        }
        // sleep on self, wait for children to exit
        sleep(p, &wait_lock);
    }
    
}

// caller must hold wake_lock
void reparent(struct proc *p)
{
    struct proc *np;
    for(np = proc; np < &proc[NPROC]; np++) {
        if(np->parent == p) {
            np->parent = initp;
            wakeup(initp);
        }
    }
}

// do not return 
void exit(int xstate) 
{
    struct proc *p = myproc();
    assert(p != initp);

    // Close all open files.
    for(int fd = 0; fd < NOFILE; fd++){
        if(p->ofile[fd]){
            struct file *f = p->ofile[fd];
            fileclose(f);
            p->ofile[fd] = 0;
        }
    }

    begin_op();
    iput(p->cwd);
    end_op();
    p->cwd = 0;

    lock_spin(&wait_lock);
    reparent(p);
    wakeup(p->parent);

    lock_spin(&p->lk);
    p->xstate = xstate;
    p->state = ZOMBIE;

    unlock_spin(&wait_lock);

    // never to return with p->lk held
    sched();
    error("zombie proc returning");
}

int kill(int pid)
{
    struct proc *p;
    for(p = proc; p < &proc[NPROC]; p++){
        lock_spin(&p->lk);
        if(p->pid == pid) {
            p->killed = true;
            // Wake process from sleep().
            if(p->state == SLEEPING){
                p->state = RUNNABLE;
            }
            unlock_spin(&p->lk);
            return 0;
        }
        unlock_spin(&p->lk);
    }
    return -1;
}

int fork()
{
    struct proc *p = myproc();
    struct proc *np = make_proc(p->name);

    // Copy user memory from parent to child.
    uvmcopy(p->pagetable, np->pagetable, p->sz);
    np->sz = p->sz;
    // copy saved user registers.
    *(np->trapframe) = *(p->trapframe);
    // Cause fork to return 0 in the child.
    np->trapframe->a0 = 0;

    // increment reference counts on open file descriptors.
    for(int i = 0; i < NOFILE; i++) {
        if(p->ofile[i]) {
            np->ofile[i] = filedup(p->ofile[i]);
        }
    }
    np->cwd = idup(p->cwd);

    int pid = np->pid;
    unlock_spin(&np->lk);

    lock_spin(&wait_lock);
    np->parent = p;
    unlock_spin(&wait_lock);

    lock_spin(&np->lk);
    np->state = RUNNABLE;
    unlock_spin(&np->lk);

    return pid;
}

// A fork child's very first scheduling by scheduler()
// will swtch to forkret.
void forkret(void)
{
    static int first = 1;

    // Still holding p->lock from scheduler.
    unlock_spin(&myproc()->lk);

    if (first) {
        // File system initialization must be run in the context of a
        // regular process (e.g., because it calls sleep), and thus cannot
        // be run from main().
        first = 0;
        init_log();
    }

    usertrapret();
}

void backtrace()
{
  printf("\nbacktrace:\n");
  uint64 fp = r_fp();
  uint64 now_fp = fp;
  while(1) {
    uint64 rtaddr = *(uint64 *)(fp - 8);
    fp = *(uint64 *)(fp - 16);
    if(fp < PGROUNDDOWN(now_fp) || fp > PGROUNDUP(now_fp))
      break;
    printf("%p\n", rtaddr);
  }
}