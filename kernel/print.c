#include "defs.h"
#include "vargs.h"
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "lock.h"

volatile int on_error = false;

// parse int into buffer in reverse order
// return length 
static int parse_int(char *buf, int base, int num)
{
    int i = 0;
    int neg = num < 0 ? true : false;
    if( num == 0 ) {
        buf[i++] = '0';
        return i;
    }
    if(neg) {
        num = -num;
    }
    while (num) {
        char c = num % base + '0';
        if(c > '9') {
            c = num % base - 10 + 'a';
        }
        buf[i++] = c;
        num /= base;
    }
    if(neg) {
        buf[i++] = '-';
    }
    return i;
}

static int parse_uint(char *buf, int base, uint32 num)
{
    int i = 0;
    if( num == 0 ) {
        buf[i++] = '0';
        return i;
    }
    while (num) {
        char c = num % base + '0';
        if(c > '9') {
            c = num % base - 10 + 'a';
        }
        buf[i++] = c;
        num /= base;
    }
    return i;
}

static void print_int(int base, int sign, int n)
{
    char buf[MAX_INT_LEN];
    memset(buf, 0, MAX_INT_LEN);
    int len;
    if(sign) {
        len = parse_int(buf, base, n);
    } else {
        len = parse_uint(buf, base, n);
    }
    if( base == 16 ) {
        consputc('0');
        consputc('x');
    }
    for(int i = len-1; i>=0; i--) {
        consputc(buf[i]);
    }
}

static void print_pointer(uint64 p)
{
    // console_putc_color(back, fore, '0');
    // console_putc_color(back, fore, 'x');
    print_int(16, 0, p);
}


static void print(char *fmt, va_list args)
{
    char c, *s;

    // lock here
    if (fmt == 0)
        error("fmt error\n");
    for(int i=0; (c = fmt[i] & 0xff) != 0; i++) {
        if(c != '%') {
            consputc(c);
            continue;
        }
        c = fmt[++i] & 0xff;
        if( c == 0 ) {
            break;
        }
        switch (c)
        {
        case 'd':       // to decimal
            print_int(10, 1, va_arg(args, int));
            break;
        case 'x':       // to hex
            print_int(16, 1, va_arg(args, int));
            break;
        case 'p':       // to uint64 pointer
            print_pointer(va_arg(args, uint64));
            break;
        case 's':
            if( (s = va_arg(args, char*)) == 0) {
                s = "(null)";
            }
            for( ; *s; s++ ) {
                consputc(*s);
            }
            break;
        case '%':
            consputc('%');
            break;
        default:
            consputc('%');
            consputc(c);
            break;
        }

    }

    // unlock
}

void printf(char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    print(fmt, args);

    va_end(args);
}

void log(char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    print("[log]: ", 0);
    print(fmt, args);

    va_end(args);
}

void error(char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);


    backtrace();
    print("[error]: ", 0);
    print(fmt, args);
    print("\n", 0);
    
    on_error = true;
    for(;;){
        ;
    }

    va_end(args);
}

struct spinlock mark_lk;
static int mark_count = 0;
void mark()
{
    if(mark_lk.name == NULL) {
        init_spinlock(&mark_lk, "mark lock");
    }
    lock_spin(&mark_lk);
    printf("-------------mark %d-------------\n", mark_count++);
    unlock_spin(&mark_lk);
}