#ifndef _TYPES_H
#define _TYPES_H

typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int  uint32;
typedef unsigned long uint64;

typedef uint64  pte_t;
typedef uint64 *pagetable_t;
typedef struct trapframe* trapframe_t;

#ifndef NULL
#define NULL    0
#endif

#define true    1
#define false   0


#endif
